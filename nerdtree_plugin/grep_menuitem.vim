"shove this in ~/.vim/nerdtree_plugin/grep_menuitem.vim
"
"A really rough integration of :grep with nerdtree. Adds a 'g' menu item that
"prompts the user for a search pattern to use with :grep. :grep is run on the
"selected dir (using the parent if a file is selected)
"
" Originally written by scrooloose
" (http://gist.github.com/205807)
 
if exists("g:loaded_nerdtree_grep_menuitem")
    finish
endif

let g:loaded_nerdtree_grep_menuitem = 1

if !exists('g:nerdtree_replace_silent')
  let g:nerdtree_replace_silent = 1
endif
 
call NERDTreeAddMenuItem({
            \ 'text': '(g)rep directory',
            \ 'shortcut': 'g',
            \ 'callback': 'NERDTreeGrep' })

call NERDTreeAddMenuItem({
            \ 'text': '(r)eplace in directory',
            \ 'shortcut': 'r',
            \ 'callback': 'NERDTreeReplace' })

command! -nargs=0 -bar Qargs execute 'args ' . QuickfixFilenames()
function! QuickfixFilenames()
  " Building a hash ensures we get each buffer only once
  let buffer_numbers = {}
  for quickfix_item in getqflist()
    let buffer_numbers[quickfix_item['bufnr']] = bufname(quickfix_item['bufnr'])
  endfor
  return join(values(buffer_numbers))
endfunction

function! NERDTreeReplace()
  let dirnode     = g:NERDTreeDirNode.GetSelected()

  let pattern     = input("Enter the search pattern: ")
  let iswin       = has('win32') || has('win64')
  if pattern == ''
      echo 'Aborted'
      return
  endif
  let pattern     = escape(pattern, "'")
  let pattern_reg = escape(pattern, "/")
  let replace     = input("Enter to replace: ")
  let replace     = escape(replace, "'/")

  if g:nerdtree_replace_silent
    let remod = 'g'
  else
    let remod = 'gc'
  endif

  "use the previous window to jump to the first search result
  call FunToggleCurrentNERDDir()
  wincmd w

  "a hack for *nix to make sure the output of "grep" isnt echoed in vim
  if !iswin
    let old_shellpipe = &shellpipe
    let &shellpipe='&>'
  endif

  try
      exec 'silent cd ' . dirnode.path.str()
      exec "silent args! `grep -lsrE '" . pattern . "' * --include=*.{php,js,css}` | argdo! %s/" . pattern_reg . "/" . replace . "/" . remod . " | argdo! update"
  finally
      if !iswin
        let &shellpipe = old_shellpipe
      endif
  endtry

  let hits = len(getqflist())
  if hits == 0
      echo "No hits"
  endif
endfunction
 
function! NERDTreeGrep()
  let dirnode = g:NERDTreeDirNode.GetSelected()

  let pattern = input("Enter the search pattern: ")
  let iswin   = has('win32') || has('win64')
  if pattern == ''
      echo 'Aborted'
      return
  endif

  "use the previous window to jump to the first search result
  call FunToggleCurrentNERDDir()
  wincmd w

  "a hack for *nix to make sure the output of "grep" isnt echoed in vim
  if !iswin
    let old_shellpipe = &shellpipe
    let &shellpipe='&>'
  endif

  try
      exec 'silent cd ' . dirnode.path.str()
      let pattern     = escape(pattern, "'")
      exec "silent grep! '" . pattern . "' * --include=*.{php,js,css} | copen"
  finally
      if !iswin
        let &shellpipe = old_shellpipe
      endif
  endtry

  let hits = len(getqflist())
  if hits == 0
      echo "No hits"
  endif
endfunction
