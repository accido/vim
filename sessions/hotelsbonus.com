let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/locale/src/hotelsbonus/app/services
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +1 ~/locale/src/hotelsbonus/www/index.php
badd +1 ~/locale/src/hotelsbonus/composer.json
badd +1 ~/locale/src/hotelsbonus/app/components/ActionRequestCancelDialog.php
badd +62 ~/locale/src/hotelsbonus/app/components/Controller.php
badd +1 ~/locale/src/hotelsbonus/app/components/Stat.php
badd +15 ~/locale/src/hotelsbonus/app/models/VwDictCity.php
badd +164 ~/locale/src/hotelsbonus/app/models/VwDictAllocation.php
badd +1 ~/locale/src/hotelsbonus/app/services/Period.php
badd +69 ~/locale/src/hotelsbonus/app/services/EWebBrowser.php
badd +0 ~/locale/src/hotelsbonus/app/services/RSA.php
silent! argdel *
edit ~/locale/src/hotelsbonus/app/services/RSA.php
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 23) / 46)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOI
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
