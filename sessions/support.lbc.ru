let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/locale/src/support.lbc.ru/www
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +0 ~/locale/src/support.lbc.ru/www/index.php
badd +1 ~/locale/src/support.lbc.ru/app.php
badd +1 ~/locale/src/support.lbc.ru/www/.htaccess
badd +1 ~/locale/src/support.lbc.ru/misc/init.php
badd +6 ~/locale/src/support.lbc.ru/templ/login.html
badd +1 ~/locale/src/support.lbc.ru/misc/config.php
badd +97 ~/locale/src/support.lbc.ru/misc/lsf_config.php
badd +29 ~/locale/src/support.lbc.ru/code/Controller.php
badd +280 ~/locale/src/support.lbc.ru/code/Support/Hotline/Moderation/Tools.php
badd +66 ~/locale/src/support.lbc.ru/code/Controller/Hotline/Moderation.php
badd +415 ~/locale/src/support.lbc.ru/code/Controller/Hotline/Ajax.php
badd +47 ~/locale/src/support.lbc.ru/templ/Controller/Hotline/Moderation/edit.html
badd +54 ~/locale/src/support.lbc.ru/code/Controller/Hotline/Control.php
badd +373 ~/locale/src/support.lbc.ru/www/js/Controller/Hotline/moderation.js
badd +1 ~/locale/src/support.lbc.ru/templ/Controller/Hotline/Control/incoming.html
badd +295 ~/locale/src/support.lbc.ru/www/css/Hotline/moderation.css
badd +5 ~/locale/src/support.lbc.ru/code/Controller/Ajax.php
badd +1 ~/locale/src/support.lbc.ru/www/.htaccess.sample
badd +23 ~/locale/src/support.lbc.ru/templ/Controller/Hotline/Control/promo.html
badd +51 ~/locale/src/support.lbc.ru/templ/Controller/Hotline/Moderation/new.html
badd +92 ~/locale/src/support.lbc.ru/misc/lsf_config_local.php
badd +4 ~/locale/src/support.lbc.ru/logs/errors/general.log
badd +281 ~/locale/src/support.lbc.ru/logs/dump.log
badd +20 ~/usr/src/support.lbc.ru/trunk/code/Controller/Translate/Tasks.php
badd +193 ~/usr/src/support.lbc.ru/trunk/code/Support/Translate/UI/FilterFactory.php
badd +16 ~/usr/src/support.lbc.ru/trunk/code/Sprt/SL.php
badd +20 ~/usr/src/support.lbc.ru/trunk/code/Controller/Call.php
badd +206 ~/usr/src/support.lbc.ru/trunk/code/Controller/Call/Tasks.php
badd +165 ~/usr/src/support.lbc.ru/trunk/code/Support/Call/Model/Domain/Task/Storage.php
badd +5 ~/usr/src/support.lbc.ru/trunk/templ/Controller/Call/Tasks/new.html
badd +502 ~/usr/src/Automation/trunk/Lspassport/Model/Staff/Base.php
badd +35 ~/usr/src/support.lbc.ru/trunk/templ/Controller/Call/Tasks/add.html
badd +26 ~/usr/src/support.lbc.ru/trunk/code/Sprt/Controller.php
badd +19 ~/usr/src/support.lbc.ru/trunk/code/Frame/Default.php
badd +209 ~/usr/src/support.lbc.ru/trunk/code/Component/TopMenu.php
badd +62 ~/usr/src/support.lbc.ru/trunk/code/Controller/Hotline/Email.php
badd +22 ~/usr/src/support.lbc.ru/trunk/code/Controller/Hotline/HotelsCheck.php
badd +1 ~/usr/src/support.lbc.ru/trunk/templ/Controller/Hotline/Errors/index.html
badd +7 ~/usr/src/support.lbc.ru/trunk/templ/Controller/Hotline/Email/index.html
badd +1 ~/usr/src/support.lbc.ru/trunk/templ/Component/TabMenu.html
badd +2 ~/usr/src/support.lbc.ru/trunk/code/Component/TabMenu.php
badd +14 ~/usr/src/support.lbc.ru/trunk/code/Controller/Login.php
badd +5 ~/usr/src/support.lbc.ru/trunk/code/Module/Email/Component/Hotline.php
badd +3 ~/usr/src/support.lbc.ru/trunk/code/Module/Email.php
badd +1 ~/usr/src/support.lbc.ru/trunk/templ/Module/Email/Component/Hotline.html
badd +1 ~/locale/src/support.lbc.ru/code/Sprt/Tools.php
badd +16 ~/locale/src/support.lbc.ru/www/css/Hotline/email.css
silent! argdel *
edit ~/locale/src/support.lbc.ru/www/index.php
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 23) / 46)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOI
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
