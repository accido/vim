let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/locale/src/git.ls1.ru/www
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +24 ~/locale/src/git.ls1.ru/www/index.php
badd +11 ~/locale/src/git.ls1.ru/README.md
badd +46 ~/locale/src/git.ls1.ru/.vimprj/init_project.vim
badd +19 ~/locale/src/git.ls1.ru/composer.json
badd +53 ~/locale/src/git.ls1.ru/www/.htaccess
badd +24 ~/locale/src/git.ls1.ru/www/.htaccess.sample-prod
badd +11 ~/locale/src/git.ls1.ru/app/config/params.php
badd +42 ~/locale/src/git.ls1.ru/vendor/yiisoft/yii/framework/YiiBase.php
badd +4 ~/locale/src/git.ls1.ru/app/globals.php
badd +42 ~/locale/src/git.ls1.ru/app/config/dev.php
badd +74 ~/locale/src/git.ls1.ru/app/config/main.php
badd +5 ~/locale/src/git.ls1.ru/app/views/site/index.php
badd +8 ~/locale/src/git.ls1.ru/app/controllers/SiteController.php
badd +1 ~/locale/src/git.ls1.ru/app/views/site/some.php
badd +26 ~/locale/src/git.ls1.ru/app/views/layouts/column2.php
badd +1 ~/locale/src/git.ls1.ru/app/components/PhpAuthManager.php
badd +95 ~/locale/src/git.ls1.ru/vendor/composer/ClassLoader.php
badd +25 ~/locale/src/git.ls1.ru/vendor/2amigos/yiistrap/composer.json
badd +21 ~/locale/src/git.ls1.ru/vendor/2amigos/yiiwheels/composer.json
badd +28 ~/locale/src/git.ls1.ru/vendor/2amigos/yiistrap/components/TbApi.php
badd +33 ~/locale/src/git.ls1.ru/app/modules/admin/components/ControllerAdmin.php
badd +9 ~/locale/src/git.ls1.ru/app/config/auth.php
badd +57 ~/locale/src/git.ls1.ru/app/modules/admin/controllers/DefaultController.php
badd +52 ~/locale/src/git.ls1.ru/app/modules/admin/models/LoginForm.php
badd +25 ~/locale/src/git.ls1.ru/app/modules/admin/components/UserIdentity.php
badd +3 ~/locale/src/git.ls1.ru/app/modules/admin/views/default/index.php
badd +1 ~/locale/src/git.ls1.ru/app/data/db.backup
badd +70 ~/locale/src/git.ls1.ru/vendor/yiisoft/yii/demos/blog/protected/data/schema.mysql.sql
badd +26 ~/locale/src/git.ls1.ru/vendor/yiisoft/yii/demos/blog/protected/models/Tag.php
badd +45 ~/locale/src/git.ls1.ru/app/modules/news/models/News.php
badd +212 ~/locale/src/git.ls1.ru/vendor/yiisoft/yii/framework/db/ar/CActiveRecord.php
badd +111 ~/locale/src/git.ls1.ru/vendor/yiisoft/yii/framework/base/CModel.php
badd +59 ~/locale/src/git.ls1.ru/app/modules/news/models/Category.php
badd +21 ~/locale/src/git.ls1.ru/app/modules/news/components/Wfilter.php
badd +26 ~/locale/src/git.ls1.ru/app/modules/admin/AdminModule.php
badd +27 ~/locale/src/git.ls1.ru/app/modules/admin/components/AdminNav.php
badd +7 ~/locale/src/git.ls1.ru/app/modules/admin/components/views/controls_module.php
badd +142 ~/locale/src/git.ls1.ru/app/gii/mymodel/templates/default/model.php
badd +115 ~/locale/src/git.ls1.ru/app/gii/mymodel/MymodelCode.php
badd +12 ~/locale/src/git.ls1.ru/app/gii/mymodel/MymodelGenerator.php
badd +56 ~/locale/src/git.ls1.ru/app/extensions/images/BehaviorModel.php
badd +1 ~/locale/src/git.ls1.ru/app/config/imagecache.php
badd +16 ~/locale/src/git.ls1.ru/www/icache.php
badd +1 ~/locale/src/git.ls1.ru/app/modules/news/views/default/index.php
badd +1 ~/locale/src/git.ls1.ru/app/modules/news/views/default/view.php
badd +1 ~/locale/src/git.ls1.ru/app/modules/news/NewsModule.php
badd +44 ~/locale/src/git.ls1.ru/app/modules/news/controllers/DefaultController.php
silent! argdel *
edit ~/locale/src/git.ls1.ru/www/index.php
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
silent! normal! zE
let s:l = 24 - ((23 * winheight(0) + 25) / 51)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
24
normal! 0
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOI
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
