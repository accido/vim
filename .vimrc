function! GetAsyncText(temp_file_name)
  echomsg readfile(a:temp_file_name)[0]
  call delete(a:temp_file_name)
endfunction

function! <SID>SendAsyncText(command)
  call <SID>SendAsync(a:command,'GetAsyncText')
endfunction

function! <SID>SendAsync(command,callback)
  let temp_file     = tempname()
  let tool_cmd      = a:command.' > '.temp_file
  let tool_cmd      = escape(tool_cmd,"'\"")
  silent exec '!start /b cmd /c "'.tool_cmd.'" & vim --servername '.v:servername." --remote-expr \"".a:callback."('".temp_file."')\""
endfunction

let s:iswin = has('win32') || has('win64')

if s:iswin
  " set default 'runtimepath' (without ~/.vim folders)
  let &runtimepath = printf('%s/vimfiles,%s,%s/vimfiles/after', $VIM, $VIMRUNTIME, $VIM)

  " parent gvim.exe directory for plugins
  let s:portable = fnamemodify($VIM,':h')

  " add the directory to 'runtimepath'
  let &runtimepath = printf('%s,%s,%s/after,%s,%s/after', s:portable, &runtimepath, s:portable, $HOME, $HOME)
else
  let &runtimepath = printf('%s/.vim,%s,%s,%s/.vim/after,%s/.vim/vimfiles', $HOME, $VIM, $VIMRUNTIME, $HOME, $HOME)
endif

let $LANG ='en'

execute pathogen#infect()
execute pathogen#helptags()

"-------------------------
" Базовые настройки
"-------------------------

filetype plugin indent on

" Начинать обзор с каталога текущего буфера
set browsedir=buffer

" Change directory to the current buffer when opening files.
set autochdir

" Включение диалогов с запросами
set confirm

" Color scheme
set background=light
colorscheme hemisu
"distinguished
let g:darkburn_high_Contrast = 1
let g:darkburn_alternate_Error = 1
let g:darkburn_alternate_Include = 1
let g:darkburn_force_dark_Background = 1

""""""""""""""""""""""""""""""""""""""""""""""
let g:thematic#defaults = {
\   'background'    : 'dark',
\   'airline-theme' : 'badwolf',
\   'ruler'         : 0,
\   'laststatus'    : 2,
\   'transparency'  : 100,
\}
let g:thematic#theme_name = 'hemisu'
""""""""""""""""""""""""""""""""""""""""""""""

" Включаем несовместимость настроек с Vi (ибо Vi нам и не понадобится).
set nocompatible

" Показывать положение курсора всё время.
set ruler

" Включаем нумерацию строк
"set nu

" Disable folding becouse folding the hight load editor
set nofoldenable

" Фолдинг по отсупам
set foldmethod=manual

" Не учитывать регистр при поиске.
set ignorecase

" Учитывать его только, когда есть хотя бы одна большая буква.
set smartcase

" Поиск по набору текста (очень полезная функция)
set incsearch

" Enable highlight the search results.
set hlsearch

" Теперь нет необходимости передвигать курсор к краю экрана, чтобы подняться в режиме редактирования
set scrolljump=7

" Теперь нет необходимости передвигать курсор к краю экрана, чтобы опуститься в режиме редактирования
set scrolloff=7

" Выключаем надоедливый "звонок"
set novisualbell
set t_vb=

" Поддержка мыши
set mouse=a
set mousemodel=popup

" Не выгружать буфер, когда переключаемся на другой
" Это позволяет редактировать несколько файлов в один и тот же момент без необходимости сохранения каждый раз
" когда переключаешься между ними
set hidden

" Включение горизонтального скролл-бара
set guioptions+=b

" Скрыть панель в gui версии ибо она не нужна
set guioptions-=T

" Сделать строку команд высотой в одну строку
set ch=1

" Скрывать указатель мыши, когда печатаем
set mousehide

" Включить автоотступы
set autoindent

" Влючить подстветку синтаксиса
syntax on

" Преобразование Таба в пробелы
set expandtab

" Размер табулации по умолчанию
set shiftwidth=2
set softtabstop=2
set tabstop=2

" Показывать по умлочанию строку со вкладками
set showtabline=1

" Отключение приветственного сообщения
set shortmess+=I

" Довсвечивать совпадающую скобку
set showmatch

" Gorizontal scroll
set sidescroll=1
set nowrap

" Grep
set grepprg=grep\ -nsrE\ $*
set grepformat=%f:%l%m
"command! -nargs=+ RGrep execute 'silent grep! <args>' | cwindow | redraw!
function! GrepWord()
  let iswin           = has('win32') || has('win64')
  let pattern         = escape(expand("<cword>"), "'")
  let pattern         = substitute(pattern, "\\((\\|)\\)", "^\\1", 'g')
  if !iswin
    let old_shellpipe = &shellpipe
    let &shellpipe='&>'
  endif
  try
    exec 'silent cd! ' . $INDEXER_PROJECT_ROOT
    exec "silent grep! '" . pattern . "' * | copen"
  finally
    if !iswin
      let &shellpipe  = old_shellpipe
    endif
  endtry
endfunction

nmap <silent> <Leader>vv :call GrepWord()<cr>
vmap <silent> <Leader>vv <esc>:call GrepWord()<cr>
imap <silent> <Leader>vv <esc>:call GrepWord()<cr>

nmap <silent> <Leader><Enter> :LocateFile <c-r>=expand('<cword>')<cr><cr>
vmap <silent> <Leader><Enter> <esc>:LocateFile <c-r>=expand('<cword>')<cr><cr>
imap <silent> <Leader><Enter> <esc>:LocateFile <c-r>=expand('<cword>')<cr><cr>
nmap <silent> <Leader><Leader> :LocateFile<cr>

let g:phpcs_std_list="PSR2"

"Syntastic
let g:syntastic_php_checkers = ['phpcs', 'phpmd']
let g:syntastic_mode_map =  { 'mode': 'passive',
                            \ 'active_filetypes': ['ruby'],
                            \ 'passive_filetypes': ['php', 'puppet', 'javascript'] }
" JSHint
let g:jshint2_close = 0
let g:jshint2_height = 20
let g:jshint2_save = 0
let g:jshint2_read = 0
inoremap <silent><M-F12> <C-O>:call JsCompileUploadOnSave()<CR>
nnoremap <silent><M-F12> :call JsCompileUploadOnSave()<CR>
vnoremap <silent><M-F12> :call JsCompileUploadOnSave()<CR>
cnoremap <M-F12> :call JsCompileUploadOnSave()<CR>
inoremap <silent><C-F12> <C-O>:call StaticAnalyzer()<CR>
nnoremap <silent><C-F12> :call StaticAnalyzer()<CR>
vnoremap <silent><C-F12> :call StaticAnalyzer()<CR>
cnoremap <C-F12> :call StaticAnalyzer()<CR>

let g:static_analyzer_task=''
function! StaticAnalyzer()
  if &filetype == 'php'
    exec 'Validate'
    lopen
  endif
endfunction

function! StaticFix(temp_file_name)
  let data=readfile(a:temp_file_name)
  let errors=[]
  let find=1
  let index=0
  while find!=-1
    let find=match(data, '\v^\=\=\=+$', index)
    let index=find+1
    if find=-1
      break
    endif
    let file=substitute(g:static_analyzer_task.'\\'.data[index-2], '\\\\', '\\', 'g')
    while find!=-1
      let find=match(data[index],'\v^Line [0-9]+\:.+$')
      if find==-1
        let find=1
        break
      endif
      let err=matchlist(data[index],'\v^Line ([0-9]+)\:(.+)$')
      call add(errors, err[2].' in '.file.' on line '.err[1])
      let index=index+1
    endwhile
  endwhile
  call delete(a:temp_file_name)
  cexpr errors
  copen
endfunction

function! JsCompileUploadOnSave()
  if &filetype != 'javascript'
    exec 'Phpcs'
    return
  endif
  if exists('g:project_session_name') && g:project_session_name != ''
    execute "mksession! ~/.vim/vimfiles/sessions/" . g:project_session_name
    execute "wviminfo! ~/.vim/vimfiles/infos/" . g:project_session_name
  endif
  let f=expand("%:p")
  let r=''
  let rr=''
  if exists("g:project_remote_host") && g:project_remote_host != ''
    let r=escape( $INDEXER_PROJECT_ROOT, '\')
    let rr=escape( g:project_remote_root, '\')
  endif
  let t=tempname()
  let l='/var/www/jsfw/www/assets/core/macros/lambda/macros/index.js'
  let c='/var/www/jsfw/www/assets/core/macros/core/index.js'
  silent write!
  silent call system('sjs -c -o '.shellescape(t).' -m '.shellescape(l).' -m '.shellescape(c).' '.shellescape(f))
  silent call system("uglifyjs ".shellescape(t)." -c -m -b -r '$,require,model,view,controller,autoloader,include,include_once,source' -o ".shellescape(t))
  "call system('sjs -c --output '.shellescape(t).' --module '.shellescape(c).' '.shellescape(t))
  exec 'vnew '.t.' | set ft=javascript'
  if r != ''
    let r=substitute(r, '\(^[\/\\]\+\)\|\([\/\\]\+$\)', '', 'g')
    let rr=substitute(rr, '\(^[\/\\]\+\)\|\([\/\\]\+$\)', '', 'g')
    let remote_path=substitute( f, r, rr, 'g')
    let remote_path=substitute( remote_path, '\', '/', 'g')
    execute 'Nwrite '
    \ . g:project_remote_host . '//'
    \ . remote_path
  endif
  exec 'SyntasticCheck'
  exec 'wincmd l | e %'
endfunction

" ==============================================================================
" "Statusline"               Статусная строка {{{1
" ==============================================================================

" Включение отображения незавершенных команд в статусной строке
set showcmd

" Всегда отображать статусную строку
set laststatus=2

hi StatusLineBufferNumber guifg=fg guibg=bg gui=bold

hi User1 guifg=#ffdad8  guibg=#880c0e
hi User2 guifg=#000000  guibg=#F4905C
hi User3 guifg=#292b00  guibg=#f4f597
hi User4 guifg=#112605  guibg=#aefe7B
hi User5 guifg=#051d00  guibg=#7dcc7d
hi User7 guifg=#ffffff  guibg=#880c0e gui=bold
hi User8 guifg=#ffffff  guibg=#5b7fbb
hi User9 guifg=#ffffff  guibg=#810085
hi User0 guifg=#ffffff  guibg=#094afe

" Формат статусной строки
set stl+=\ %#StatusLineBufferNumber#
set stl+=[%n]    " Номер буфера
set stl+=%*
set stl+=\ %<%F      " Имя файла загруженного в буфер
set stl+=%#identifier#[%{&ft} " Тип файла, загруженного в буфер, например [cpp]
set stl+=\|
set stl+=%{&fileencoding} " Кодировка файла
set stl+=\|
set stl+=%{&ff}] " Формат файла
set stl+=%#Statement#%{fugitive#statusline()}%* " Repository and Branch
set stl+=%=      " Выравнивание по правому краю
set stl+=\ Line:
set stl+=%#PreProc#\ %l\#%3v/   " Номер строки
set stl+=%L      " Количество строк в буфере
set stl+=\ [%P]  " Позиция текста, отображаемого в окне
                 " по отношению к началу файла в процентах
set stl+=%#error#%m%*       " Флаг состояния несохранённых изменений
set stl+=%#warningmsg#%r%*  " Флаг состояния 'только для чтения

" Показ нескольких типов предупреждений связанных с некорректной работой с
" отступами:
" "[mixed-indenting]" - В случае если в файле для задания отступов совместно
" используются пробелы и символы табуляции
" "[expandtab-disabled]" - Если опция expandtab отключена, но в файле для
" задания отступов применяются пробелы
" "[expandtab-enabled]" - Если опция expandtab активна, но для задания
" отступов используются символы табуляции
set statusline+=%#error#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%*

" Включаем "умные" отспупы ( например, автоотступ после {)
set smartindent

" Fix <Enter> for comment
set fo+=cr

" Опции сесссий
set sessionoptions=curdir,buffers,folds,tabpages,winpos,help

" Авто-сохранение сессии при закрытии vim`a (загрузка F12 в норм. режиме)
function! SleepSession()
  if exists('g:project_session_name') && g:project_session_name != ''
    execute "mksession! ~/.vim/vimfiles/sessions/" . g:project_session_name
    execute "wviminfo! ~/.vim/vimfiles/infos/" . g:project_session_name
  else
    execute "mksession! ~/.vim/lastsession.vim"
    execute "wviminfo! ~/.vim/_viminfo"
  endif
endfunction

" Загрузка последней сессии
function! LoadSession()
  let filename = expand('%:p')
  if 0 == strlen(filename)
    execute "source ~/.vim/lastsession.vim"
    execute "rviminfo ~/.vim/_viminfo"
  elseif exists('g:project_session_name') && g:project_session_name != ''
    execute "source ~/.vim/vimfiles/sessions/" . g:project_session_name
    execute "rviminfo ~/.vim/vimfiles/infos/" . g:project_session_name
    execute 'edit! ' . filename
  endif
endfunction

if has("autocmd")
  autocmd VimEnter * nested call LoadSession()
  autocmd VimEnter * silent NERDTree
  autocmd VimEnter * nested TagbarOpen
  autocmd VimLeavePre * silent call SleepSession()
endif

"-------------------------
" Горячие клавишы
"-------------------------


" Пробел в нормальном режиме перелистывает страницы
nmap <Space> <PageDown>

" CTRL-F для omni completion
imap <C-F> <C-X><C-O>
"imap <M-F> <C-X><C-U>

" C-c and C-v - Copy/Paste в "глобальный клипборд"

"<Ctrl-C> -- copy (goto visual mode and copy)
imap <C-C> <C-O>vgG
vmap <C-C> "*y<Esc>i

"<Ctrl-A> -- copy all
imap <C-A> <C-O>gg<C-O>gH<C-O>G<Esc>
vmap <C-A> <Esc>gggH<C-O>G<Esc>i

"<Ctrl-V> -- paste
nm \\paste\\ "=@*.'xy'<CR>gPFx"_2x:echo<CR>
imap <C-V> x<Esc>\\paste\\"_s
vmap <C-V> "-cx<Esc>\\paste\\"_x

" Заставляем shift-insert работать как в Xterm
map <S-Insert> <MiddleMouse>

" C-y - удаление текущей строки
nmap <C-y> dd
imap <C-y> <esc>ddi

" C-d - дублирование текущей строки
imap <C-d> <esc>yypi

" Поиск и замена слова под курсором
nmap ; :%s/\<<c-r>=expand("<cword>")<cr>\>/

nmap <silent> Ü a<Space>λ
vmap <silent> Ü <esc>a<Space>λ
imap <silent> Ü λ
nmap <silent> ® a->
vmap <silent> ® <esc>a->
imap <silent> ® ->
nmap <silent> ¯ a=>
vmap <silent> ¯ <esc>a=>
imap <silent> ¯ =>



" F2 - быстрое сохранение
nmap <silent> <F2> :call SaveSession()<cr>
vmap <silent> <F2> <esc>:call SaveSession()<cr>i
imap <silent> <F2> <esc>:call SaveSession()<cr>i

nmap <silent> <M-F2> :call FunUploadOnSave()<cr>
vmap <silent> <M-F2> <esc>:call FunUploadOnSave()<cr>i
imap <silent> <M-F2> <esc>:call FunUploadOnSave()<cr>i

nmap <silent> <M-F3> :call RemoteLoad()<cr>
vmap <silent> <M-F3> <esc>:call RemoteLoad()<cr>i
imap <silent> <M-F3> <esc>:call RemoteLoad()<cr>i

function! SaveSession()
  write
  if exists('g:project_session_name') && g:project_session_name != ''
    execute "mksession! ~/.vim/vimfiles/sessions/" . g:project_session_name
    execute "wviminfo! ~/.vim/vimfiles/infos/" . g:project_session_name
  endif
endfunction

function! FunUploadOnSave()
  if exists("g:project_remote_host") && g:project_remote_host != ''
    let f=expand("%:p")
    let r=escape( $INDEXER_PROJECT_ROOT, '\')
    let r=substitute(r, '\(^[\/\\]\+\)\|\([\/\\]\+$\)', '', 'g')
    let rr=escape( g:project_remote_root, '\')
    let rr=substitute(rr, '\(^[\/\\]\+\)\|\([\/\\]\+$\)', '', 'g')
    let remote_path=substitute( f, r, rr, 'g')
    let remote_path=substitute( remote_path, '\', '/', 'g')
    execute 'Nwrite '
    \ . g:project_remote_host . '//'
    \ . remote_path
  endif
  write
  if exists('g:project_session_name') && g:project_session_name != ''
    execute "mksession! ~/.vim/vimfiles/sessions/" . g:project_session_name
    execute "wviminfo! ~/.vim/vimfiles/infos/" . g:project_session_name
  endif
endfunction

function! RemoteLoad()
  if exists("g:project_remote_host") && g:project_remote_host != ''
    let f=expand("%:p")
    let r=escape( $INDEXER_PROJECT_ROOT, '\')
    let r=substitute(r, '\(^[\/\\]\+\)\|\([\/\\]\+$\)', '', 'g')
    let rr=escape( g:project_remote_root, '\')
    let rr=substitute(rr, '\(^[\/\\]\+\)\|\([\/\\]\+$\)', '', 'g')
    let remote_path=substitute( f, r, rr, 'g')
    let remote_path=substitute( remote_path, '\', '/', 'g')
    let ft=&filetype
    execute 'vert diffs '.g:project_remote_host.'//'.remote_path
    execute 'set filetype=' . ft
  endif
endfunction

" Windows jumping
nmap <silent> <c-left> :wincmd W<cr>
nmap <silent> <c-right> :wincmd w<cr>
nmap <silent> <c-up> :wincmd k<cr>
nmap <silent> <c-down> :wincmd j<cr>

" F4 - закрыть текущий буфер
nmap <silent> <F4> :bd<cr>:diffoff<cr>:set nowrap<cr>
vmap <silent> <F4> <esc>:bd<cr>:diffoff<cr>:set nowrap<cr>
imap <silent> <F4> <esc>:bd<cr>:diffoff<cr>:set nowrap<cr>

nmap <silent> <M-F5> :wincmd j<cr>:quit<cr>
vmap <silent> <M-F5> <esc>:wincmd j<cr>:quit<cr>
imap <silent> <M-F5> <esc>:wincmd j<cr>:quit<cr>i

" F5 - просмотр списка буферов
nmap <silent> <F5> <esc>:BufExplorer<cr>
vmap <silent> <F5> <esc>:BufExplorer<cr>
imap <silent> <F5> <esc>:BufExplorer<cr>

let g:bufExplorerShowDirectories=1
let g:bufExplorerShowRelativePath=1

" F6 - предыдущий буфер
map <silent> <F6> :bp<cr>
vmap <silent> <F6> <esc>:bp<cr>i
imap <silent> <F6> <esc>:bp<cr>i

" F7 - следующий буфер
map <silent> <F7> :bn<cr>
vmap <silent> <F7> <esc>:bn<cr>i
imap <silent> <F7> <esc>:bn<cr>i

" F8 - список закладок
map <silent> <F8> :MarksBrowser<cr>
vmap <silent> <F8> <esc>:MarksBrowser<cr>
imap <silent> <F8> <esc>:MarksBrowser<cr>

" F9 - "Project" команда
nmap <silent> <F9> :call FunAddBookmark()<cr>
imap <silent> <F9> :call FunAddBookmark()<cr>
vmap <silent> <F9> :call FunAddBookmark()<cr>

" Добавить закладку для текущего файла
function! FunAddBookmark()
  if exists("g:NERDTreeBookmarks")
    let l:bname=expand("<cword>")
    let l:bpath=g:NERDTreePath.New(expand("%:p"))
    call g:NERDTreeBookmark.AddBookmark(l:bname,l:bpath)
    call g:NERDTreeBookmark.Write()
    NERDTree
    call NERDTreeRender()
  endif
endfunction

" Hit enter in the file browser to open the selected
" file with :vsplit to the right of the browser.
let g:netrw_preview=1
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_banner=0
let g:netrw_hide=1

" Toggle Vexplore with Ctrl-E
function! ToggleVExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec 'wincmd w'
      Vexplore
      let t:expl_buf_num = bufnr("%")
  endif
endfunction

" F10 - показать/спрятать файловый менеджер
map <silent> <F10> :call FunToggleCurrentNERDDir(  )<cr>
vmap <silent> <F10> <esc>:call FunToggleCurrentNERDDir(  )<cr>
imap <silent> <F10> <esc>:call FunToggleCurrentNERDDir(  )<cr>

" Устанавливаем то что нам нужно - рабочий каталог для текущего файла
function! FunToggleCurrentNERDDir(  )
  if !exists( "b:isOpenNERDBuf" )
   let b:isOpenNERDBuf=0
  endif
  if b:isOpenNERDBuf == 1
    NERDTreeClose
    let b:isOpenNERDBuf=0
  else
    NERDTree
    "call NERDTreeRender(  )
    let b:isOpenNERDBuf=1
  endif
endfunction

" F11 - показать окно Tagbar
map <silent> <F11> :TagbarToggle<cr>
vmap <silent> <F11> <esc>:TagbarToggle<cr>
imap <silent> <F11> <esc>:TagbarToggle<cr>

" F12 - просмотр ошибок
nmap <silent><F12> :SyntasticCheck<cr>:Errors<cr>
vmap <silent><F12> <esc>:SyntasticCheck<cr>:Errors<cr>
imap <silent><F12> <c-o>:SyntasticCheck<cr>:Errors<cr>
cmap <F12> :SyntasticCheck<cr>:Errors<cr>

" Показывать окно слева
let g:tagbar_left=1

" Ширина окна
let g:tagbar_width=23

" Показывать +/-
let g:tagbar_iconchars=['+', '-']

" Не сортировать
let g:tagbar_sort=0

" Авто фокус
let g:tagbar_autofocus=1
let g:tagbar_autoshowtag=1

" После выбора закрыть окно и переключится на выбранный элемент
let g:tagbar_autoclose=0
let g:tagbar_singleclick = 1

" Убираем все лишнее
let g:tagbar_compact=1
let g:tagbar_indent=1
let g:tagbar_foldlevel=0

" Видимость
let g:tagbar_show_visibility=1

" < & > - делаем отступы для блоков
vmap < <gv
vmap > >gv

" Выключаем ненавистный режим замены
imap <Ins> <Esc>i

" Меню выбора кодировки текста (koi8-r, cp1251, cp866, utf8)
set wildmenu
set wcm=<Tab>
menu Encoding.koi8-r :e ++enc=koi8-r<CR>
menu Encoding.windows-1251 :e ++enc=cp1251<CR>
menu Encoding.cp866 :e ++enc=cp866<CR>
menu Encoding.utf-8 :e ++enc=utf8 <CR>

" Редко когда надо [ без пары =)
" imap [ <c-r>=FunQuote("[")<cr>
" Аналогично и для {
" imap {<CR> {<CR>}<Esc>O
" imap (<cr> (<cr>)<left><cr><Esc>kA<tab>
" Аналогично и для (
" inoremap ( <c-r>=FunQuote("(")<cr>
" inoremap " <c-r>=FunQuote("\"")<cr>
" inoremap ' <c-r>=FunQuote("'")<cr>
func FunQuote(char)
  let cur=getline(".")[col(".") - 1]
  let ret=a:char
  let m=match( cur, '\s\|\n\|\r') != -1 || cur == ''
  if a:char == "'" && ( m || match( cur, '\w' ) == -1 )
    let ret .="'\<left>"
  elseif a:char == "\"" && ( m || match( cur, '\w' ) == -1 )
    let ret .="\"\<left>"
  elseif a:char == "[" && m
    let ret .="]\<left>"
  elseif a:char == "(" && m
    let ret .=")\<left>"
  endif
  return ret
endfunc
" С-q - выход из Vim
map <C-Q> <Esc>:qa<cr>

" allow to use backspace instead of "x"
set backspace=indent,eol,start whichwrap+=<,>,[,]

" Удаление всего, что в парных символах
" Символ для поиска определяется под курсором
func FunBackspace()
  if col('.') == 1
    if line('.')  != 1
      return  "\<esc>kA\<del>"
    else
      return ""
    endif
  else
    execute "normal \<left>"
    let cur=getline(".")[col(".") - 1]
    if strlen(cur) == 1 && strridx( "{}[]()'\"`<>", cur) != -1
      execute "normal di" . cur . "a"
      return ""
    else
      return "\<del>"
    endif
  endif
endfunc
inoremap <M-BS> <c-r>=FunBackspace()<cr>

" Автозавершение слов по tab =)
function! InsertTabWrapper()
  let col = col('.') - 1
  let last=getline('.')
  if !col || last[col-1] !~ '\v[0-9a-zA-Z.>:_]'
    return "\<tab>"
  else
    "if last[col-1] =~ '\v[.>:]'
    "  return "\<c-x>\<c-o>"
    "endif
    return "\<c-x>\<c-o>"
  endif
endfunction
imap <silent><tab> <c-r>=InsertTabWrapper()<cr>
"imap <silent><tab> <c-x><c-o>

" Слова откуда будем завершать
set complete=""
" Из текущего буфера
set complete+=.
" Из словаря
set complete+=k
" Из других открытых буферов
set complete+=b
" из тегов
set complete+=t

" Закрывать автоматически теги в стиле html
let b:closetag_html_style=1

" Включаем filetype плугин. Настройки, специфичные для определынных файлов мы разнесём по разным местам
filetype plugin on
au BufRead,BufNewFile *.phps set filetype=php
au BufRead,BufNewFile *.thtml set filetype=php
au Filetype html,xml,xsl source $VIMRUNTIME/scripts/closetag.vim

" Настройки для Tlist (показвать только текущий файл в окне навигации по коду)
let g:Tlist_Show_One_File = 1
let g:Tlist_File_Fold_Auto_Close = 1

set completeopt-=preview
set completeopt+=longest
set mps-=[:]

" ECLIM - ECLIPSE IDE
function! StartEclimd()
  if $ECLIM_APPLICATION_COUNTER==0
    let command='e:\utils\eclipse\eclimd'
    call <SID>SendAsync(command,'StaticFix')
    let $ECLIM_APPLICATION_COUNTER=1
  endif
endfunction

inoremap <silent><m-`> <esc>:call StartEclimd()<cr>i
nnoremap <silent><m-`> :call StartEclimd()<cr>
vnoremap <silent><m-`> <esc>:call StartEclimd()<cr>
let g:EclimCompletionMethod='omnifunc'
let g:airline_section_c='%#User8#%{EclimStatusLine()}%<%t'
let g:EclimLocateFileDefaultAction='edit'
"let g:EclimPhpSearchSingleResult='edit'
let g:EclimProjectTabTreeAutoOpen=0

function! EclimStatusLine()
  let status=eclim#project#util#ProjectStatusLine()
  if status!=''
    return ' '.status.'///'
  endif
  return ''
endfunction

" INDEXER options
let g:indexer_ctagsJustAppendTagsAtFileSave=1

" PDV - PHPDocumentator
let g:pdv_cfg_Uses = 1

" PHP documenter script bound to Control-P
let g:pdv_template_dir = $HOME . "/.vim/templates/core"
inoremap <c-p> <ESC>:call pdv#DocumentCurrentLine()<CR>i
nnoremap <c-p> :call pdv#DocumentCurrentLine()<CR>
vnoremap <c-p> :call pdv#DocumentCurrentLine()<CR>

inoremap <c-w> <ESC>:call pdv#DocumentWithSnip()<CR>i

set winminheight=0
" всегда делать активное окно максимального размера
set noequalalways
set lines=99999 columns=99999
" autocmd VimEnter * execute ":simalt ~Р"
" установить шрифт
"set guifont=Anonymous_Pro:h10:cRUSSIAN
set guifont=ProFontWindows:h10:cRUSSIAN
" настраиваю для работы с русскими словами (чтобы w, b, * понимали
" русские слова)
set iskeyword=@,48-57,_,192-255
" перенос по словам, а не по буквам
set linebreak
set linespace=2
set dy=lastline
set encoding=utf-8
set termencoding=utf-8
set fileencodings=utf-8,cp1251

 " Формат файла по умолчанию
set fileformat=unix         
set fileformats=unix,dos,mac

if has("autocmd")
    autocmd BufRead *.sql set filetype=mysql  
endif

if version >= 700
    set history=64
    set undolevels=128
    set undodir=~/.vim/undodir/
    set undofile
    set undolevels=1000
    set undoreload=10000
endif

" ==============================================================================
" "Plugin.NERDTree"         {{{1
" ==============================================================================

" Установить положение окна NERDTree, "left" или "right"
let NERDTreeWinPos='right'
let NERDTreeAutoCenter=0
let NERDTreeQuitOnOpen=0
let NERDTreeDirArrows=0

let NERDTreeShowBookmarks=1
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\~$', '\.swp$', '\.swo$', '\.git$', '\.project$', '\.settings$', '\.buildpath$', '\.idea$', '^\.$', '^\.\.$']
let NERDChristmasTree=0

" Функция возвращает "[mixed-indenting]" если для отступов в
" файле совместно используются пробелы и символы табуляции.
" Если же выставлена опция "expandtab" и в файле присутствуют
" символы табуляции то возвращаемое значение будет "[expandtab-enabled]".
" Так же в противоположном случае: если опция "expandtab" не задана
" но в файле для отступов используются пробелы возвращаемым
" значением будет "[expandtab-disabled]". В остальных случаях
" результатом функции будет пустая строка
function! StatuslineTabWarning()
    if !exists("b:statusline_tab_warning")
        let b:statusline_tab_warning = ''

        if !&modifiable
            return b:statusline_tab_warning
        endif

        let tabs = search('^\t', 'nw') != 0

        "find spaces that arent used as alignment in the first indent column
        let spaces = search('^ \{' . &ts . ',}[^\t]', 'nw') != 0

        if tabs && spaces
            let b:statusline_tab_warning = '[mixed-indenting]'
        elseif (spaces && !&expandtab)
            let b:statusline_tab_warning = '[expandtab-disabled]'
        elseif (tabs && &expandtab)
            let b:statusline_tab_warning = '[expandtab-enabled]'
        else
            let b:statusline_tab_warning = ''
        endif
    endif
    return b:statusline_tab_warning
endfunction

function! BufList()
    let status = ""
    for i in range(1, last_buffer_nr()+1)
        if bufnr("%") == i
            let status = status . '   ' . '[' . bufname(i) . ']' "объединяем строки
            continue
        endif
        if buflisted(i)
            let status = status . '   ' . bufname(i)
        endif
    endfor
    return status
endfunction
" Search documentation in the browser
" Inspiration: http://vim.wikia.com/wiki/Online_documentation_for_word_under_cursor
 
function! Browser()
    if has("win32") || has("win64")
        let s:browser = "H:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"
    elseif has("win32unix") " Cygwin
        let s:browser = "'/cygdrive/c/Program\ Files/Mozilla\ Firefox/firefox.exe' -new-tab"
    elseif has("mac") || has("macunix") || has("unix")
        let s:browser = "firefox -new-tab"
    endif
  
    return s:browser
endfunction
 
function! Run(command)
    if has("win32") || has("win64")
        let s:startCommand = "!start"
        let s:endCommand = ""
    elseif has("mac") || has("macunix") " TODO Untested on Mac
        let s:startCommand = "!open -a"
        let s:endCommand = ""
    elseif has("unix") || has("win32unix")
        let s:startCommand = "!"
        let s:endCommand = "&"
    else
        echo "Don't know how to handle this OS!"
        finish
    endif
 
    let s:cmd = "silent " . s:startCommand . " " . a:command . " " . s:endCommand
    " echo s:cmd
    execute s:cmd
endfunction
 
function! OnlineDoc( mode )
  if a:mode == ''
    let l:ft = &filetype
  else
    let l:ft = a:mode
  endif

    if l:ft == "viki"
        let s:urlTemplate = "http://dictionary.reference.com/browse/<name>"
    elseif l:ft == "perl"
        let s:urlTemplate = "http://perldoc.perl.org/functions/<name>.html"
    elseif l:ft == "python"
        let s:urlTemplate = "http://www.google.com/search?q=<name>&domains=docs.python.org&sitesearch=docs.python.org"
    elseif l:ft == "ruby"
        let s:urlTemplate = "http://www.ruby-doc.org/core/classes/<name>.html"
    elseif l:ft == "vim"
        let s:urlTemplate = "http://vimdoc.sourceforge.net/search.php?search=<name>&docs=help"
    elseif l:ft == "php"
        let s:urlTemplate = "file:///" . $VIM . "/php/docs/syntax/function.<name>.html"
    elseif l:ft == "wp"
        let s:urlTemplate = "http://codex.wordpress.org/Function_Reference/<name>"
    endif

    if l:ft == "php"
      let s:wordUnderCursor = tolower(substitute( expand("<cword>"), '_', '-', 'g'))
    else
      let s:wordUnderCursor = expand( "<cword>" )
    endif

    let s:url = substitute(s:urlTemplate, '<name>', s:wordUnderCursor, 'g')
 
    call Run(Browser() . " " . s:url)
endfunction
 
noremap <silent> <M-d> :call OnlineDoc('')<CR>
inoremap <silent> <M-d> <Esc>:call OnlineDoc('')<CR>a
noremap <silent> <M-x> :call OnlineDoc( "wp" )<cr>
inoremap <silent> <M-x> <esc> :call OnlineDoc( "wp" )<cr>a

" Highlight all instances of word under cursor, when idle.
" Useful when studying strange source code.
" Type z/ to toggle highlighting on/off.
nnoremap z/ :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>

function! AutoHighlightToggle()
  let @/ = ''
  if exists('#auto_highlight')
    au! auto_highlight
    augroup! auto_highlight
    setl updatetime=4000
    echo 'Highlight current word: off'
    return 0
  else
    augroup auto_highlight
      au!
      au CursorHold * let @/ = '\V\<'.escape(expand('<cword>'), '\').'\>'
    augroup end
    setl updatetime=500
    echo 'Highlight current word: ON'
    return 1
  endif
endfunction

" ex command for toggling hex mode - define mapping if desired
command -bar Hexmode call ToggleHex()

" helper function to toggle hex mode
function! ToggleHex()
  " hex mode should be considered a read-only operation
  " save values for modified and read-only for restoration later,
  " and clear the read-only flag for now
  let l:modified=&mod
  let l:oldreadonly=&readonly
  let &readonly=0
  let l:oldmodifiable=&modifiable
  let &modifiable=1
  if !exists("b:editHex") || !b:editHex
    " save old options
    let b:oldft=&ft
    let b:oldbin=&bin
    " set new options
    setlocal binary " make sure it overrides any textwidth, etc.
    let &ft="xxd"
    " set status
    let b:editHex=1
    " switch to hex editor
    %!xxd
  else
    " restore old options
    let &ft=b:oldft
    if !b:oldbin
      setlocal nobinary
    endif
    " set status
    let b:editHex=0
    " return to normal editing
    %!xxd -r
  endif
  " restore values for modified and read only state
  let &mod=l:modified
  let &readonly=l:oldreadonly
  let &modifiable=l:oldmodifiable
endfunction
