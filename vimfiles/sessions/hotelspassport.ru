let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +1 ~/usr/src/hotelspassport.ru/trunk/www/index.php
badd +1 ~/locale/src/hp.dev/www/index.php
badd +1 ~/locale/src/hp.dev/install.php
badd +14 ~/locale/src/hp.dev/code/Admin/Controller/NewUsers.php
badd +1 ~/locale/src/hp.dev/code/Hp/Db/User.php
badd +9 ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription.php
badd +78 ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
badd +4 ~/usr/src/hotelspassport.ru/trunk/code/Hp/Db/Tariff/DirectoryW.php
args ~/locale/src/hp.dev/www/index.php
edit ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
silent! normal! zE
let s:l = 78 - ((22 * winheight(0) + 23) / 46)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
78
normal! 0
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOI
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
