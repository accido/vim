# Этот файл viminfo автоматически создан Vim 7.4.
# Его можно (осторожно!) редактировать.

# Значение опции 'encoding' в момент записи файла
*encoding=utf-8


# hlsearch on (H) or off (h):
~h
# Последний Шаблон поиска:
~MSle0~/log

# Последний Замена Шаблон поиска:
~MSle0&\<self\>

# Последняя строка для замены:
$static

# Командная строка, история (начиная от свежего к старому):
:qa
:Qa
:ProjectCreate %:p:h -p hotelsbonus -n php,javascript
:e
:43
:ProjectInfo
:w
:diffoff
:diffth
:difth
:ProjectCreate %:p:h -p PEAR -n php,javascript
:ProjectDelete PEAR
:ProjectDelete pear
:39
:423
:lclose
:50
:qw
:%s/\<self\>/static/gc
:cclose
:lopen
:%s/\<USER_SALT\>/PASS
:1287
:1284
:copen
:args! `grep! 'travelpassport' *` 
:args! `grep! 'travelpassport' *`
:args! `grep! 'travelpassport' * --include=*.{php,js,css}`
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo %s/travelpassport/travelpassport
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo cat
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo echo
:grep! 'travelpassport' * --include=*.{php,js,css}
:%s/\->travelpassport/->travelpassport
:%s/\v->travelpassport/->travelpassport
:%s/\v-\>travelpassport/->travelpassport
:%s/\v\->travelpassport/->travelpassport
:%s/\<load_tag\>/get_tag/gc
:%s/\<teg_key\>/tag_key/gc
:%s/\\v\\->travelpassport/->travelpassport
:%s/\\v\->travelpassport/->travelpassport
:%s/->travelpassport/->travelpassport
:ProjectCreate %:p:h -p accido -n php,javascript
:ProjectCreate %:p:h -p accido -n php,javascript,html
:PingEclim
:ProjectCreate %:p:h -p accido -n php
:ProjectDelete accido
:%s/\<id_tag\>/create_tag/gc
:%s/\<teg_key\>/tag_key
:ProjectNatureAdd accido php
:ProjectNatureAdd php
:ProjectNatureAdd pph
:ProjectNatureAdd 
:EclimDisable
:vnew
:ProjectNatureAdd jsfw js
:ProjectNatureAdd jsfw javascript
:ProjectNatureAdd javascript
:ProjectNatureAdd
:q
:qa!
:%s/\<clear_both\>/clear
:%s/\<\>/qa
:363

# Строка поиска, история (начиная от свежего к старому):
? ^\s*\(5\)\>
?/log
?/delete
? ^\s*\(104\)\>
? ^\s*\(75\)\>
?/icache
?/->table
? ^\s*\(116\)\>
?/default
? ^\s*\(109\)\>
? ^\s*\(26\)\>
? ^\s*\(23\)\>
?/#action
?/date_promo
? ^\s*\(27\)\>
?/->_parts
? ^\s*\(28\)\>
? ^\s*\(29\)\>
? ^\s*\(24\)\>
? ^\s*\(19\)\>
?/date
?/Date
?/timestamp
? ^\s*\(82\)\>
?/bootst
?/button
?/subm
?/submit
?/form
? ^\s*\(279\)\>
? ^\s*\(285\)\>
? ^\s*\(291\)\>
?/summary
?/grid
?/subscriber
?/day
?/ajax
? ^\s*\(274\)\>
? ^\s*\(165\)\>
?/activetextfi
?/tbhtml
?/tdhtml
?/inputAddOn
?/prepend
?/icon
?/whd
? ^\s*\(1\)\>
?/setlangua
?/->run(
?/onbeginrequest
?/getla
?/travel
?/auto
? ^\s*\(245\)\>
?/nav-tabs
?/custom
?/**
? ^\s*\(234\)\>
? ^\s*\(228\)\>
? ^\s*\(224\)\>
? ^\s*\(206\)\>
? ^\s*\(207\)\>
?/gii
? ^\s*\(200\)\>

# Выражение, история (начиная от свежего к старому):

# Строка ввода, история (начиная от свежего к старому):
@/home/accido/locale/src/stat.hp.com/www/loader.php
@/home/accido/locale/src/hotline.hp.com/www/loader.php
@yes
@/home/accido/locale/src/hotelscheck/app/modules/admin/assets/js/admin.js
@/home/accido/locale/src/hotelscheck/app/modules/admin/assets/js/
@/home/accido/locale/src/hotelscheck/app/components/WebModule.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/subscriber/SummaryAction.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/subscriber/MonthAction.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/subscriber/
@/home/accido/locale/src/hotelscheck/app/components/behaviors/DateBehavior.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/subscriber/index.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/subscriber/
@/home/accido/locale/src/hotelscheck/app/components/StrapModule.php
@/home/accido/locale/src/accido/www/core/cache/driver.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/column2.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/2column,php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/SubscriberController.php
@/home/accido/locale/src/hotelscheck/www/robots.txt
@/home/accido/locale/src/hotelscheck/app/views/layouts/admin.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/main.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/
@/home/accido/locale/src/hotelscheck/app/modules/admin/components/Controller.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/components/
@RecordBehavior
@recordbehavior
@/home/accido/locale/src/hotelscheck/app/components/UserBehavior.php
@->register
@\->register_model
@->travelpassport
@\->travelpassport
@-\>travelpassport
@->_allocation
@\->_allocation
@\-\>travelpassport
@->register_model
@-\>register_model
@/var/www/accido/www/core/type.php
@/var/www/accido/www/core/formatter/gz.php
@/var/www/accido/www/core/config.php
@/home/accido/usr/share/applications/eclipse.desktop
@/var/www/jsfw/www/assets/core/models/test.js
@/home/accido/locale/src/hotelscheck/app/models/AboutForm.php
@/home/accido/locale/src/hotelscheck/app/views/layouts/static.php
@/home/accido/locale/src/hotelscheck/app/components/RecordCommandBuilder.php
@/home/accido/locale/src/hotelscheck/app/components/RecordCommandBuilder,php
@/home/accido/locale/src/hotelscheck/app/views/about/index.php
@/home/accido/locale/src/hotelscheck/app/views/about/
@/home/accido/locale/src/hotelscheck/app/controllers/AboutController.php
@/home/accido/locale/src/hotelscheck/app/views/search/index.php
@/home/accido/locale/src/hotelscheck/app/views/search/
@/home/accido/locale/src/hotelscheck/app/controllers/SearchController.php
@/home/accido/locale/src/hotelscheck/app/views/promo/index.php
@/home/accido/locale/src/hotelscheck/app/views/promo/promo.php
@/home/accido/locale/src/hotelscheck/app/views/promo/
@/home/accido/locale/src/hotelscheck/app/controllers/PromoController.php
@/home/accido/locale/src/hotelscheck/app/views/ajax/info.php
@/home/accido/locale/src/hotelscheck/app/controllers/ajax/InfoAction.php
@/home/accido/locale/src/hotelscheck/app/controllers/ajax/article/SubscribeAction.php
@/home/accido/locale/src/hotelscheck/app/components/behaviors/EmbedBehavior.php
@/home/accido/locale/src/hotelscheck/app/components/behaviors/
@/home/accido/locale/src/hotelscheck/app/components/widgets/Share.php
@/home/accido/locale/src/hotelscheck/app/views/widgets/sort.php
@/home/accido/locale/src/hotelscheck/app/components/widgets/Sort.php
@/home/accido/locale/src/hotelscheck/app/components/SortBehavior.php

# Строка ввода, история (начиная от свежего к старому):

# Регистры:
"0	LINE	0
	/**
	 * Class: heredoc
	 *
	 * @package Model
	 * 
	 * @author andrew scherbakov <accido@ukr.net>
	 * @version $id$
	 * @copyright © 2014 andrew scherbakov
	 * @license MIT http://opensource.org/licenses/MIT
	 */
"1	LINE	0
	$app->run();
"2	LINE	0
	                array(
	                    'class' => 'CProfileLogRoute',
	                    'enabled' => YII_DEBUG && isset($_REQUEST['debug']),
	                    'levels' => 'trace, info, profile, warning, error',
	                    'categories' => array('system.*', 'ar.*'),
	                ),
	                array(
	                    'class' => 'CEmailLogRoute',
	                    'levels' => 'error, warning',
	                    'except' => 'exception.CHttpException.*',
	                    // add your email in this section
	                    'emails' => array('hotline@bugs.lightsoft.ru'),
	                ),
"3	LINE	0
			'db' => array(
			    'connectionString' => 'pgsql:host=192.168.150.21;dbname=db0',
			    'emulatePrepare' => true,
			    'username' => 'hotline',
			    'password' => 'hotlineadmin',
			    'charset' => 'utf8',
			    'enableProfiling' => true,
			    'schemaCachingDuration' => 3600,
			    'enableParamLogging' => true
			),
"4	LINE	0
	dd        'vendor.2amigos.yiistrap.helpers.TbHtml',
"5	LINE	0
	        'bootstrap' => realpath(__DIR__ . '/../../vendor/2amigos/yiistrap'), // change this if necessary
	        'yiiwheels' => realpath(__DIR__ . '/../../vendor/2amigos/yiiwheels'), // change this if necessary
"6	LINE	0
	    
"7	LINE	0
	Yii::createWebApplication(
"8	LINE	0
	require_once __DIR__ . '/../app/app/WebApplication.php';
	Yii::createWebApplication($config)->run();
"9	LINE	0
	Yii::createWebApplication($config)->run();
"a@	CHAR	0
	1255
<adie('test�kr�kr;�k2dd�k2�k5�kd�kd�kd�kd�kd�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�kdOFs					.fail(function(�kr{�kr�kr;�kd�kd�kd�kd�kd�kd�kd�kd�kd�ku�ku�ku�ku�kdOF�kb				.fail(function(�kr{�kr�kr;�kl�ku�kl�kd�kl�ku�ku�ku�ku�ku�ku�ku�kr�kr�kr�kr					�ku�kr�ku						_this.resst�kb�kbet(�kr;�kd�kd�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�k5�kd�ku�kd�ku:h macro�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku:�kb�k4:h macros�kl�kl�kl�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�k4:h record�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�k4
"e	LINE	0
	
	let g:EasyGrepFileAssociations='I:/workspace/vim/vim73/plugin/EasyGrepFileAssociations'
	let g:EasyGrepMode=3
	let g:EasyGrepCommand=1
	let g:EasyGrepRecursive=0
	let g:EasyGrepSearchCurrentBufferDir=0
	let g:EasyGrepIgnoreCase=0
	let g:EasyGrepHidden=0
	let g:EasyGrepFilesToExclude=''
	let g:EasyGrepAllOptionsInExplorer=1
	let g:EasyGrepWindow=0
	let g:EasyGrepReplaceWindowMode=0
	let g:EasyGrepOpenWindowOnMatch=1
	let g:EasyGrepEveryMatch=0
	let g:EasyGrepJumpToMatch=1
	let g:EasyGrepInvertWholeWord=0
	let g:EasyGrepFileAssociationsInExplorer=0
	let g:EasyGrepExtraWarnings=0
	let g:EasyGrepOptionPrefix='<leader>vy'
	let g:EasyGrepReplaceAllPerFile=0
"y	LINE	0
			//$( "#tabs .element.from input.from, #tabs .element.to input.to" ).autocomplete({
				//source:"/includes/cities.php?lang="+lang+"",
				//minLength: 3,
				//delay: 0
			//});
	        $html .= '<a style="' .
	            $border . 'background-color: #' .
	            $color . ' !important;" href="#" title="' .
	            $this->label . '" class="text-center btn btn-small">' .
	            $this->count . '</a>';
""-	CHAR	0
	/

# Глобальные отметки:
'J  818  19  I:\workspace\vim\_vimrc
'K  818  19  I:\workspace\vim\_vimrc
'0  1  0  ~/locale/src/hotelsbonus/app/services/RSA.php
'1  7  0  ~/locale/src/support.lbc.ru/www/index.php
'2  156  0  ~/locale/src/hotline.hp.com/app/runtime/application.log
'3  6  23  ~/locale/src/hotline.hp.com/app/config/console.php
'4  30  0  ~/locale/src/hotline.hp.com/app/yiic.php
'5  10  13  ~/locale/src/hotline.hp.com/app/config/console.php
'6  40  12  ~/locale/src/hotline.hp.com/app/config/console.php
'7  44  8  ~/locale/src/hotline.hp.com/app/config/console.php
'8  32  4  ~/locale/src/hotline.hp.com/app/config/console.php
'9  28  0  ~/locale/src/hotline.hp.com/app/config/console.php

# Список прыжков (сначала более свежие):
-'  1  0  ~/locale/src/hotelsbonus/app/services/RSA.php
-'  69  0  ~/locale/src/hotelsbonus/app/services/EWebBrowser.php
-'  1  0  ~/locale/src/hotelsbonus/app/services/Period.php
-'  164  0  ~/locale/src/hotelsbonus/app/models/VwDictAllocation.php
-'  15  0  ~/locale/src/hotelsbonus/app/models/VwDictCity.php
-'  1  0  ~/locale/src/hotelsbonus/app/components/Stat.php
-'  62  0  ~/locale/src/hotelsbonus/app/components/Controller.php
-'  1  0  ~/locale/src/hotelsbonus/app/components/ActionRequestCancelDialog.php
-'  1  0  ~/locale/src/hotelsbonus/composer.json
-'  4  0  ~/locale/src/hotelsbonus/app/app/[BufExplorer]
-'  1  0  ~/locale/src/hotelsbonus/app/app/[BufExplorer]
-'  1  0  ~/locale/src/hotelsbonus/www/index.php
-'  7  0  ~/locale/src/support.lbc.ru/www/index.php
-'  269  0  ~/locale/src/support.lbc.ru/code/Support/Hotline/Moderation/Tools.php
-'  2  0  ~/locale/src/support.lbc.ru/www/index.php
-'  156  0  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  102  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CLogRouter.php
-'  1  0  ~/locale/src/hotline.hp.com/www/[BufExplorer]
-'  100  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CLogRouter.php
-'  77  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CLogRouter.php
-'  90  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CFileLogRoute.php
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/[BufExplorer]
-'  144  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CFileLogRoute.php
-'  70  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CFileLogRoute.php
-'  35  9  ~/locale/src/hotline.hp.com/app/config/console.php
-'  86  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/base/CApplication.php
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/base/[BufExplorer]
-'  47  44  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/console/CConsoleApplication.php
-'  115  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/console/CConsoleApplication.php
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/console/CConsoleApplication.php
-'  28  0  ~/locale/src/hotline.hp.com/app/yiic.php
-'  43  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  1  0  ~/locale/src/hotline.hp.com/app/config/[BufExplorer]
-'  1224  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/db/ar/CActiveRecord.php
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/db/ar/[BufExplorer]
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/db/ar/CActiveRecord.php
-'  37  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  52  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  267  0  ~/locale/src/hotline.hp.com/app/commands/HotelsnewsCommand.php
-'  1  0  ~/locale/src/hotline.hp.com/app/commands/[BufExplorer]
-'  1  0  ~/locale/src/hotline.hp.com/app/runtime/[BufExplorer]
-'  6  22  ~/locale/src/hotline.hp.com/app/config/console.php
-'  30  0  ~/locale/src/hotline.hp.com/app/yiic.php
-'  1  0  ~/locale/src/hotline.hp.com/app/[BufExplorer]
-'  29  0  ~/locale/src/hotline.hp.com/app/yiic.php
-'  1  0  ~/locale/src/hotline.hp.com/app/yiit.php
-'  28  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  10  12  ~/locale/src/hotline.hp.com/app/config/console.php
-'  40  12  ~/locale/src/hotline.hp.com/app/config/console.php
-'  164  0  ~/locale/src/hotline.hp.com/app/config/main.php
-'  199  24  ~/locale/src/hotline.hp.com/app/config/main.php
-'  206  9  ~/locale/src/hotline.hp.com/app/config/main.php
-'  217  24  ~/locale/src/hotline.hp.com/app/config/main.php
-'  179  8  ~/locale/src/hotline.hp.com/app/config/main.php
-'  21  25  ~/locale/src/hotline.hp.com/app/config/console.php
-'  44  8  ~/locale/src/hotline.hp.com/app/config/console.php
-'  212  0  ~/locale/src/hotline.hp.com/app/config/main.php
-'  32  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  25  0  ~/locale/src/hotline.hp.com/app/config/main.php
-'  22  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  19  0  ~/locale/src/hotline.hp.com/app/config/main.php
-'  79  2  ~/locale/src/hotline.hp.com/app/[BufExplorer]
-'  23  0  ~/locale/src/hotline.hp.com/app/config/main.php
-'  7  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  1  0  ~/locale/src/hotline.hp.com/app/config/local.php
-'  69  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  1  0  ~/locale/src/hotline.hp.com/app/config/_console.php
-'  132  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/base/CApplication.php
-'  85  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/base/CApplication.php
-'  58  40  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/web/CWebApplication.php
-'  9  43  ~/locale/src/hotline.hp.com/app/app/WebApplication.php
-'  125  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/YiiBase.php
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/[BufExplorer]
-'  98  2  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/YiiBase.php
-'  96  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/YiiBase.php
-'  47  0  ~/locale/src/hotelscheck/vendor/yiisoft/yii/framework/console/CConsoleApplication.php
-'  1  0  ~/locale/src/hotelscheck/vendor/yiisoft/yii/framework/console/[BufExplorer]
-'  10  50  ~/locale/src/hotline.hp.com/app/app/ConsoleApplication.php
-'  23  1  ~/locale/src/hotline.hp.com/README.md
-'  67  0  ~/locale/src/hotline.hp.com/app/config/console.php
-'  14  0  ~/locale/src/hotline.hp.com/app/config/main.php
-'  9  0  ~/locale/src/hotline.hp.com/app/models/PostSource.php
-'  1  0  ~/locale/src/hotline.hp.com/app/models/[BufExplorer]
-'  2  0  ~/locale/src/hotline.hp.com/app/app/ConsoleApplication.php
-'  18  0  ~/locale/src/hotline.hp.com/app/app/WebApplication.php
-'  1  0  ~/locale/src/hotline.hp.com/app/app/[BufExplorer]
-'  1  0  ~/locale/src/hotline.hp.com/app/app/ConsoleApplication.php
-'  15  0  ~/locale/src/hotelscheck/www/NERD_tree_1
-'  156  0  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  102  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CLogRouter.php
-'  1  0  ~/locale/src/hotline.hp.com/www/[BufExplorer]
-'  100  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CLogRouter.php
-'  77  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CLogRouter.php
-'  90  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CFileLogRoute.php
-'  1  0  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/[BufExplorer]
-'  144  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CFileLogRoute.php
-'  70  1  ~/locale/src/hotline.hp.com/vendor/yiisoft/yii/framework/logging/CFileLogRoute.php

# История местных отметок (от более свежих к старым):

> ~/locale/src/hotelsbonus/www/index.php
	"	1	0

> ~/locale/src/hotelsbonus/www/NERD_tree_1
	"	247	0
	.	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0

> ~/locale/src/hotelsbonus/www/__Tagbar__
	"	1	0
	.	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0
	+	3	0

> ~/locale/src/hotelsbonus/composer.json
	"	1	0

> ~/locale/src/hotelsbonus/app/app/WebApplication.php
	"	1	0

> ~/locale/src/hotelsbonus/app/app/[BufExplorer]
	"	1	0

> ~/locale/src/hotelsbonus/app/components/ActionRequestCancelDialog.php
	"	1	0

> ~/locale/src/hotelsbonus/app/components/Controller.php
	"	62	0

> ~/locale/src/hotelsbonus/app/components/Stat.php
	"	1	0

> ~/locale/src/hotelsbonus/app/models/VwDictCity.php
	"	15	0

> ~/locale/src/hotelsbonus/app/models/VwDictAllocation.php
	"	164	0

> ~/locale/src/hotelsbonus/app/services/Period.php
	"	1	0

> ~/locale/src/hotelsbonus/app/services/EWebBrowser.php
	"	69	0

> ~/locale/src/hotelsbonus/app/services/RSA.php
	"	1	0
