# Этот файл viminfo автоматически создан Vim 7.4.
# Его можно (осторожно!) редактировать.

# Значение опции 'encoding' в момент записи файла
*encoding=utf-8


# hlsearch on (H) or off (h):
~h
# Последний Шаблон поиска:
~MSle0~/api

# Последний Замена Шаблон поиска:
~MSle0&\<self\>

# Последняя строка для замены:
$static

# Командная строка, история (начиная от свежего к старому):
:ProjectInfo
:Git merger taks
:e
:Gblame
:copen
:cclose
:81
:156
:85
:36
:Qa
:ProjectCreate %:p:h -p hotelsbonus -n php,javascript
:43
:w
:diffoff
:diffth
:difth
:ProjectCreate %:p:h -p PEAR -n php,javascript
:ProjectDelete PEAR
:ProjectDelete pear
:39
:423
:lclose
:50
:qw
:%s/\<self\>/static/gc
:lopen
:%s/\<USER_SALT\>/PASS
:ProjectCreate %:p:h -p stat.ph.com -n php
:ProjectCreate %:p:h -p stat.ph.com -n php,js
:ProjectCreate %:p:h -p stat.ph.com -n php,javascript
:Git push --all -u
:h commenter
:GLog
:Glog
:new
:h Gcommit
:h Git
:%s/12655/13348/gc
:iab err
:ab err
:ab
:h abbr
:E
:ProjectCreate %:p:h -p hotelscheck.com -n php
:Git checkout -b dev
:Phpcs yiics
:Phpcs
:echo g:phpcs_std_list
:let g:phpcs_std_list = "yiics"
:h Phpcs
:%s/"//gc
:e ./User.php
:ProjectCreate %:p:h -p crypto -n php
:%s/\<mergeRows\>/groupColumns
:w!
:Gcommit -am "added metric system to hotline"
:%s/$field/$#/gc
:ProjectDelete git.ls1.ru
:ProjectDelete git.ls1.dev
:Git merge task
:Git checkout develop
:Git branch -av
:Git merge taks

# Строка поиска, история (начиная от свежего к старому):
?/api
?/targetpostpre
?/para
?/createMetr
?/log
?/default
? ^\s*\(329\)\>
?/min
?/length
?/string
?/validate
?/form
? ^\s*\(318\)\>
? ^\s*\(310\)\>
? ^\s*\(84\)\>
?/_GET
?/hp
? ^\s*\(98\)\>
? ^\s*\(99\)\>
?/hotel
?/feed
?/mb
?/border
?/medium
?/outline
? ^\s*\(17\)\>
?/upper
?/_partsFields
?/respo
?/getattr
? ^\s*\(141\)\>
? ^\s*\(142\)\>
?/_index
?/readall
?/cactivedata
?/real
?/allocation
?/$type
?/getrela
?/mergeWith
?/function find
?/getText
?/getDbCr
?/hl_metric_customer
?/hl_metric
? ^\s*\(106\)\>
? ^\s*\(110\)\>
?/dbhotline
?/function model
? ^\s*\(95\)\>
?/ccomp
?/content
? ^\s*\(37\)\>
?/getdbconnection
?/function with
?/hp_user
?/getPr
?/getPk
?/_query['select'
?/metricfilter
?/$target
?/source
?/lists
?/helper

# Выражение, история (начиная от свежего к старому):

# Строка ввода, история (начиная от свежего к старому):
@module
@assert
@/home/accido/locale/src/stat.hp.com/www/loader.php
@webmodule
@/var/www/accido/www/core/formatter/cvs.php
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/Text/
@ClassLoader
@/home/accido/locale/src/s7.db0.ru/code/Ra/Db/Country.php
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/City/index.html
@/home/accido/locale/src/s7.db0.ru/code/RA/
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/Collaboration.php
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Text/Collaboration.html
@hl_item_allocation_rel
@/home/accido/locale/src/stat.hp.com/app/views/metric/hppersist.php
@/home/accido/locale/src/stat.hp.com/app/controllers/metric/PersistHPassport.php
@/home/accido/locale/src/stat.hp.com/app/models/filters/MetricHPassportFilter.php
@/home/accido/locale/src/stat.hp.com/app/controllers/metric/HPassport.php
@CEvent
@CErrorEvent
@/home/accido/locale/src/stat.hp.com/app/modules/metric/helpers/ExceptionHelper.php
@/home/accido/locale/src/stat.hp.com/app/modules/metric/components/PersistBehavior.php
@fullpath
@/var/www/accido/www/core/formatter/crypt/decoder.php
@/var/www/accido/www/core/formatter/crypt/
@/var/www/accido/www/core/formatter/crypt/coder.php
@/home/accido/locale/src/stat.hp.com/app/modules/metric/helpers/ExpressionHelper.php
@gii
@/home/accido/locale/src/stat.hp.com/app/modules/metric/models/DictInfo.php
@/home/accido/locale/src/stat.hp.com/app/modules/metric/models/DictAddr.php
@/home/accido/locale/src/stat.hp.com/app/modules/metric/models/DictAllocation.php
@/home/accido/locale/src/stat.hp.com/app/modules/metric/models/HotelRel.php
@/home/accido/locale/src/stat.hp.com/app/components/widgets/GroupColumn.php
@/home/accido/locale/src/stat.hp.com/app/components/widgets/GroupGrid.php
@/home/accido/locale/src/stat.hp.com/app/assets/css/metric.css
@/home/accido/locale/src/stat.hp.com/app/modules/metric/helpers/TargetGroupHelper.php
@/var/www/accido/www/core/cache.php
@/var/www/accido/www/core/interface/cache.php
@/var/www/accido/www/core/system/sql.php
@dbHotline
@/var/www/accido/www/core/system/memcache.php
@/home/accido/locale/src/welcome/app/modules/metric/views/landing/_success.php
@CForm
@/home/accido/locale/src/welcome/app/modules/metric/models/LandingForm.php
@/home/accido/locale/src/welcome/app/modules/metric/models/
@/home/accido/locale/src/welcome/app/modules/metric/views/landing/index.php
@/home/accido/locale/src/welcome/app/modules/metric/views/landing/
@/home/accido/locale/src/welcome/app/modules/metric/views/
@/home/accido/locale/src/welcome/app/modules/metric/controllers/LandingController.php
@/home/accido/locale/src/welcome/app/modules/metric/controllers/
@/home/accido/locale/src/welcome/app/modules/metric/components/
@/home/accido/locale/src/welcome/app/modules/metric/MetricModule.php
@/home/accido/locale/src/welcome/app/modules/metric/
@YiiBase
@/home/accido/locale/src/welcome/app/modules/captcha/controllers/DefaultController.php
@/home/accido/locale/src/welcome/app/modules/captcha/controllers/
@/home/accido/locale/src/welcome/app/modules/captcha/CaptchaModule.php
@/home/accido/locale/src/welcome/app/modules/captcha/
@Form
@validate\(
@form
@bsdev
@controllerMap
@\/usr\/local\/www
@CLogger

# Строка ввода, история (начиная от свежего к старому):

# Регистры:
"0	LINE	0
	/**
	 * Class: heredoc
	 *
	 * @package Model
	 * 
	 * @author andrew scherbakov <accido@ukr.net>
	 * @version $id$
	 * @copyright © 2014 andrew scherbakov
	 * @license MIT http://opensource.org/licenses/MIT
	 */
"1	LINE	0
	        var_dump($limit);die;
"2	LINE	0
	use Yii;
	
"3	LINE	0
	        var_dump($this);
"4	LINE	0
	        die('test');
"5	LINE	0
	            array('target, content', ''),
"6	LINE	0
			return array(
	            array('item_id, id, like, user', 'numerical', 'integerOnly'=>true),
	            array('user', 'safe'),
	            array('item_id, user, id, like', 'safe', 'on'=>'search'),
			);
"7	LINE	0
		{
"8	LINE	0
		public function rules()
"9	LINE	0
		"*p"}
"a@	CHAR	0
	1255
<adie('test�kr�kr;�k2dd�k2�k5�kd�kd�kd�kd�kd�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�kdOFs					.fail(function(�kr{�kr�kr;�kd�kd�kd�kd�kd�kd�kd�kd�kd�ku�ku�ku�ku�kdOF�kb				.fail(function(�kr{�kr�kr;�kl�ku�kl�kd�kl�ku�ku�ku�ku�ku�ku�ku�kr�kr�kr�kr					�ku�kr�ku						_this.resst�kb�kbet(�kr;�kd�kd�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�k5�kd�ku�kd�ku:h macro�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku:�kb�k4:h macros�kl�kl�kl�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�k4:h record�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�k4
"e	LINE	0
	
	let g:EasyGrepFileAssociations='I:/workspace/vim/vim73/plugin/EasyGrepFileAssociations'
	let g:EasyGrepMode=3
	let g:EasyGrepCommand=1
	let g:EasyGrepRecursive=0
	let g:EasyGrepSearchCurrentBufferDir=0
	let g:EasyGrepIgnoreCase=0
	let g:EasyGrepHidden=0
	let g:EasyGrepFilesToExclude=''
	let g:EasyGrepAllOptionsInExplorer=1
	let g:EasyGrepWindow=0
	let g:EasyGrepReplaceWindowMode=0
	let g:EasyGrepOpenWindowOnMatch=1
	let g:EasyGrepEveryMatch=0
	let g:EasyGrepJumpToMatch=1
	let g:EasyGrepInvertWholeWord=0
	let g:EasyGrepFileAssociationsInExplorer=0
	let g:EasyGrepExtraWarnings=0
	let g:EasyGrepOptionPrefix='<leader>vy'
	let g:EasyGrepReplaceAllPerFile=0
"y	LINE	0
			//$( "#tabs .element.from input.from, #tabs .element.to input.to" ).autocomplete({
				//source:"/includes/cities.php?lang="+lang+"",
				//minLength: 3,
				//delay: 0
			//});
	        $html .= '<a style="' .
	            $border . 'background-color: #' .
	            $color . ' !important;" href="#" title="' .
	            $this->label . '" class="text-center btn btn-small">' .
	            $this->count . '</a>';
"-	CHAR	0
	n

# Глобальные отметки:
'J  818  19  I:\workspace\vim\_vimrc
'K  818  19  I:\workspace\vim\_vimrc
'0  42  0  ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
'1  4  7  ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
'2  14  0  ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
'3  13  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
'4  2  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
'5  14  24  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
'6  12  22  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
'7  16  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
'8  181  8  ~/locale/src/welcome/app/modules/metric/models/MetricPart.php
'9  184  19  ~/locale/src/welcome/app/modules/metric/models/MetricPart.php

# Список прыжков (сначала более свежие):
-'  42  0  ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
-'  1  0  ~/locale/src/welcome/app/modules/articles/controllers/[BufExplorer]
-'  4  7  ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
-'  14  0  ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
-'  10  7  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  13  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  2  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  14  24  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  1  0  ~/locale/src/welcome/app/modules/news/controllers/[BufExplorer]
-'  78  0  ~/locale/src/welcome/vendor/yiisoft/yii/framework/db/schema/CDbCriteria.php
-'  1  0  ~/locale/src/welcome/vendor/yiisoft/yii/framework/db/schema/[BufExplorer]
-'  8  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  12  22  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  2  0  ~/locale/src/welcome/www/index.php
-'  26  0  ~/locale/src/welcome/www/[BufExplorer]
-'  1  0  ~/locale/src/welcome/www/[BufExplorer]
-'  16  0  ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
-'  23  1  ~/locale/src/hotline.hp.com/README.md
-'  1  0  ~/locale/src/welcome/www/index.php

# История местных отметок (от более свежих к старым):

> ~/locale/src/welcome/www/index.php
	"	2	0

> ~/locale/src/hotline.hp.com/www/index.php
	"	17	0

> ~/locale/src/hotline.hp.com/README.md
	"	23	1

> ~/locale/src/welcome/app/modules/news/controllers/ApiController.php
	"	10	0
	^	15	8
	.	13	0
	+	11	26
	+	2	8
	+	11	67
	+	14	19
	+	17	0
	+	15	37
	+	12	27
	+	11	50
	+	12	28
	+	10	39
	+	11	20
	+	12	41
	+	11	39
	+	16	28
	+	11	56
	+	12	59
	+	16	0
	+	11	26
	+	2	8
	+	11	67
	+	14	19
	+	17	0
	+	15	37
	+	12	27
	+	11	50
	+	12	28
	+	10	39
	+	11	20
	+	12	41
	+	11	39
	+	16	28
	+	11	56
	+	12	59
	+	16	39
	+	11	21
	+	12	22
	+	14	28
	+	14	0
	+	2	0
	+	14	0
	+	13	0

> ~/locale/src/welcome/www/NERD_tree_1
	"	23	0
	.	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0

> ~/locale/src/welcome/www/__Tagbar__
	"	3	3
	.	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0
	+	4	0

> ~/locale/src/welcome/www/[BufExplorer]
	"	1	0

> ~/locale/src/welcome/app/modules/news/controllers/[BufExplorer]
	"	1	0

> ~/locale/src/welcome/vendor/yiisoft/yii/framework/db/schema/CDbCriteria.php
	"	78	0

> ~/locale/src/welcome/vendor/yiisoft/yii/framework/db/schema/[BufExplorer]
	"	1	0

> ~/locale/src/welcome/app/modules/articles/controllers/ApiController.php
	"	42	0
	^	4	7
	.	4	7
	+	15	0
	+	14	0
	+	4	7

> ~/locale/src/welcome/app/modules/articles/controllers/[BufExplorer]
	"	1	0
