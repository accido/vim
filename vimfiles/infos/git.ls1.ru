# Этот файл viminfo автоматически создан Vim 7.4.
# Его можно (осторожно!) редактировать.

# Значение опции 'encoding' в момент записи файла
*encoding=utf-8


# hlsearch on (H) or off (h):
~h
# Последний Шаблон поиска:
~MSle0~/ab_

# Последний Замена Шаблон поиска:
~MSle0&<?php echo \$index;\??>

# Последняя строка для замены:
$$index

# Командная строка, история (начиная от свежего к старому):
:248
:w
:qa
:%s/<?php echo \$index;\??>/$index/gc
:%s/<?php echo \$index;\?>/$index/gc
:%s/<?php echo $index;\?>/$index/gc
:390
:%s/_\d_/_<?php echo $index;?>_/gc
:set ft=php
:%s/\[\d\]/[<?php echo $index;?>]/gc
:%s/\[\d\]/<?php echo $index;?>/gc
:%s/$field/$#/gc
:%s/radio_select/tarif/gc
:ProjectCreate %:p:h -p git.ls1.ru -n php
:ProjectDelete git.ls1.ru
:ProjectDelete git.ls1.dev
:ProjectInfo
:set encoding=utf8
:set fileencoding=utf8
:set encoding=cp1251
:set fileencoding=cp1251
:%s/\<table\>/select/gc
:e
:echo &tabstop
:set tabstop=4
:set &tabstop=2
:echo &omnifunc
:echo &indentexpr
:qa!
:w ~/usr/share/applications/pgadmin.desktop
:w!
:new
:Git push --all -u
:Git remote add origin git@git.ls1.ru:sherbakov/git-ls1-ru.git
:Git remote rm origin
:Git push --all -u --force
:Git remote add origin http://git.ls1.ru/test/news_yii.git
:Git remote -v
:Git remote add http://git.ls1.ru/test/news_yii.git
:Gstatus

# Строка поиска, история (начиная от свежего к старому):
?/tabstop
?/attr
?/submit
?/setValueDirect
?/_getData
?/useDefaultsForPart
?/default
?/_value
?/setValue
?/value
? ^\s*\(35\)\>
?/_getFilePart
?/filePart
?/_fields
?/_partsFields
?/ext
?/getDefaultValue
?/_defaults
?/_getRenderData
?/getFormData
?/_getPaths
?/urlMask
?/pre_
?/_callFilter
?/_filters
?/orm
??php
?/registerEvent
?//
? ^\s*\(71\)\>
? <?php echo \$index;\??>
?/<?php echo $index;\?
? <?php echo \$index;\?>
? <?php echo $index;\?>
?/<?php
?/_prepareData
?/_customizeForm
? ^\s*\(40\)\>
? ^\s*\(31\)\>
? ^\s*\(33\)\>
? ^\s*\(13\)\>
?/description
? _\d_
? \[\d\]
?/\[\d\]
?/$#
? $field
? radio_select
?/icache
?/behaviors
?/params
?/_aliases
?/setPathOf
?/register
? ^\s*\(55\)\>
? ^\s*\(54\)\>
? ^\s*\(10\)\>
? ^\s*\(58\)\>
?/java
?/_getObje
?/_groupSets
?/addAndAdd
?/_buttons
?/fileencod

# Выражение, история (начиная от свежего к старому):

# Строка ввода, история (начиная от свежего к старому):
@yes
@/home/accido/locale/src/s7.db0.ru/cache/templ-c/
@/home/accido/locale/src/s7.db0.ru/www/icache.php
@/home/accido/locale/src/s7.db0.ru/code/Ra/Db/Image.php
@/home/accido/locale/src/git.ls1.ru/app/data/schema.postgre.sql
@/home/accido/locale/src/git.ls1.ru/app/data/news.db
@ClassLoader
@/home/accido/locale/src/s7.db0.ru/code/Ra/Db/Country.php
@/home/accido/locale/src/s7.db0.ru/code/Ra/Db/City.php
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/City/index.html
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/City/
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/City.php
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Text/Collaboration/index.html
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Text/Collaboration/
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Collaboration.html
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Collaboration.htm
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Collaboration.php
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/Collaboration.php
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Text/Collaboration.html
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Text/
@/home/accido/locale/src/s7.db0.ru/templ/Admin/Controller/Text.html
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/Text/Collaboration.php
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/Text/
@/home/accido/locale/src/s7.db0.ru/code/RA/
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/collaboration
@/home/accido/locale/src/s7.db0.ru/code/Admin/Controller/Text.php
@/home/accido/locale/src/hosting/code/S
@/home/accido/usr/share/applications/

# Строка ввода, история (начиная от свежего к старому):

# Регистры:
"0	BLOCK	59
	'/icache/{$id_' . $index . '}_300x300.{$ext_' . $index . '}'
"1	LINE	0
	  hostname 192.168.101.207
"2	LINE	0
	  identityfile ~/.ssh/id_rsa
"3	LINE	0
	                ->addFilePartField('ext_' . $index, 'ext', 'image')
"4	LINE	0
	        var_dump($data);die;
"5	LINE	0
	                    <img src="/icache/{$fields.id_3.value}_300x300.{$fields.ext_3.value}">
"6	LINE	0
	                    <img src="/icache/{$fields.id_2.value}_300x300.{$fields.ext_2.value}">
"7	LINE	0
	                    <img src="/icache/{$fields.id_1.value}_300x300.{$fields.ext_1.value}">
"8	LINE	0
	                    <img src="/icache/{$fields.id_0.value}_300x300.{$fields.ext_0.value}">
"9	LINE	0
	                    <?php var_dump($this->_data['fields']);die;?>
"a	CHAR	0
	/\[\d\]�krs3
"e	LINE	0
	
	let g:EasyGrepFileAssociations='I:/workspace/vim/vim73/plugin/EasyGrepFileAssociations'
	let g:EasyGrepMode=3
	let g:EasyGrepCommand=1
	let g:EasyGrepRecursive=0
	let g:EasyGrepSearchCurrentBufferDir=0
	let g:EasyGrepIgnoreCase=0
	let g:EasyGrepHidden=0
	let g:EasyGrepFilesToExclude=''
	let g:EasyGrepAllOptionsInExplorer=1
	let g:EasyGrepWindow=0
	let g:EasyGrepReplaceWindowMode=0
	let g:EasyGrepOpenWindowOnMatch=1
	let g:EasyGrepEveryMatch=0
	let g:EasyGrepJumpToMatch=1
	let g:EasyGrepInvertWholeWord=0
	let g:EasyGrepFileAssociationsInExplorer=0
	let g:EasyGrepExtraWarnings=0
	let g:EasyGrepOptionPrefix='<leader>vy'
	let g:EasyGrepReplaceAllPerFile=0
"y	LINE	0
			//$( "#tabs .element.from input.from, #tabs .element.to input.to" ).autocomplete({
				//source:"/includes/cities.php?lang="+lang+"",
				//minLength: 3,
				//delay: 0
			//});
""-	CHAR	0
	7

# Глобальные отметки:
'J  818  19  I:\workspace\vim\_vimrc
'K  818  19  I:\workspace\vim\_vimrc
'0  1  0  ~/locale/src/git.ls1.ru/www/__Tagbar__
'1  22  0  ~/locale/src/s7.db0.ru/code/Admin/Controller/Text/Collaboration.php
'2  248  0  /var/www/fls/Tools/ImageCache.php
'3  4  19  ~/.ssh/config
'4  1  0  /var/www/fls/App.ph
'5  165  0  ~/locale/src/s7.db0.ru/code/Admin/Controller/Apartment.php
'6  188  8  ~/locale/src/s7.db0.ru/code/Admin/Controller/Apartment.php
'7  93  7  ~/locale/src/s7.db0.ru/code/Admin/Controller/Apartment.php
'8  166  39  ~/locale/src/s7.db0.ru/code/Admin/Controller/Apartment.php
'9  52  20  ~/locale/src/s7.db0.ru/templ/Admin/Controller/Apartment/_form.html

# Список прыжков (сначала более свежие):
-'  1  0  ~/locale/src/git.ls1.ru/www/__Tagbar__
-'  8  0  ~/locale/src/git.ls1.ru/www/NERD_tree_1
-'  19  0  ~/locale/src/git.ls1.ru/www/NERD_tree_1
-'  1  0  ~/locale/src/git.ls1.ru/www/NERD_tree_1
-'  24  0  ~/locale/src/git.ls1.ru/www/index.php
-'  44  0  ~/locale/src/git.ls1.ru/app/modules/news/controllers/DefaultController.php
-'  11  0  ~/locale/src/git.ls1.ru/README.md
-'  22  0  ~/locale/src/s7.db0.ru/code/Admin/Controller/Text/Collaboration.php
-'  41  12  ~/locale/src/s7.db0.ru/code/Admin/Controller/Apartment.php
-'  6  0  ~/locale/src/s7.db0.ru/www/admin.php
-'  1  0  ~/locale/src/s7.db0.ru/code/Controller.php
-'  160  14  ~/locale/src/s7.db0.ru/templ/Controller/index.html
-'  10  0  ~/locale/src/s7.db0.ru/templ/Controller/index.html
-'  2  0  ~/locale/src/s7.db0.ru/templ/Component/FooterJS.html
-'  1  0  ~/locale/src/s7.db0.ru/code/Component/FooterJS.php
-'  80  22  ~/locale/src/s7.db0.ru/code/Admin/Controller/City.php
-'  65  0  ~/locale/src/s7.db0.ru/code/Admin/Controller/Apartment.php
-'  1  0  ~/locale/src/s7.db0.ru/www/admin.php
-'  14  1  ~/locale/src/s7.db0.ru/www/index.php
-'  30  0  ~/locale/src/s7.db0.ru/misc/lsf_config_local.php
-'  137  0  ~/locale/src/s7.db0.ru/cache/templ-c/clean_my_index.html_3451827621.php
-'  248  0  /var/www/fls/Tools/ImageCache.php
-'  1  0  /var/www/fls/Tools/[BufExplorer]
-'  101  0  /var/www/fls/Tools/ImageCache/LSFDb2TableSource.php
-'  80  0  /var/www/fls/Form/Field/Image.php
-'  1  0  /var/www/fls/Form/Field/[BufExplorer]
-'  202  0  /var/www/fls/Form/Field/File.php
-'  188  0  /var/www/fls/Form/Field/File.php
-'  94  0  /var/www/fls/Form/Field/Image.php
-'  19  0  /var/www/fls/Form/Field/Image.php
-'  481  0  /var/www/fls/Form.php
-'  1  0  /var/www/fls/[BufExplorer]
-'  839  9  /var/www/fls/Form.php
-'  73  25  /var/www/fls/Form.php
-'  1102  0  /var/www/fls/Form.php
-'  843  12  /var/www/fls/Form.php
-'  851  25  /var/www/fls/Form.php
-'  500  20  /var/www/fls/Form.php
-'  453  20  /var/www/fls/Form.php
-'  451  57  /var/www/fls/Form.php
-'  450  25  /var/www/fls/Form.php
-'  449  16  /var/www/fls/Form.php
-'  74  55  /var/www/fls/Form.php
-'  39  16  /var/www/fls/Form.php
-'  23  15  /var/www/fls/Form.php
-'  1362  22  /var/www/fls/Form.php
-'  1361  19  /var/www/fls/Form.php
-'  1357  17  /var/www/fls/Form.php
-'  1354  21  /var/www/fls/Form.php

# История местных отметок (от более свежих к старым):

> ~/locale/src/git.ls1.ru/www/index.php
	"	24	0

> ~/locale/src/git.ls1.ru/README.md
	"	11	0

> ~/locale/src/git.ls1.ru/app/modules/news/controllers/DefaultController.php
	"	44	0

> ~/locale/src/git.ls1.ru/www/NERD_tree_1
	"	8	0
	.	1	0
	+	1	0
	+	1	0

> ~/locale/src/git.ls1.ru/www/__Tagbar__
	"	1	0
	.	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
	+	7	0
