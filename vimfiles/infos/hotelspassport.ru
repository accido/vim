# Этот файл viminfo автоматически создан Vim 7.4.
# Его можно (осторожно!) редактировать.

# Значение опции 'encoding' в момент записи файла
*encoding=utf-8


# hlsearch on (H) or off (h):
~h
# Последний Шаблон поиска:
~MSle0~/pr

# Последний Замена Шаблон поиска:
~MSle0&\<self\>

# Последняя строка для замены:
$static

# Командная строка, история (начиная от свежего к старому):
:ProjectNatureAdd jsfw js
:268
:286
:w
:PingEclim
:e
:%s/\<hotlne\>/hotline
:!scp 192.168.101.207:/webhome/support.lbc.ru/misc/lsf_config_local.php ./lsf_config_local.php
:!ls ./
:!scp 192.168.101.207:/webhome/support.lbc.ru/misc/lsf_config_local.php ./misc/lsf_config_local.php
:!scp 192.168.101.207:/webhome/support.lbc.ru/misc/lsf_config_local.php ./misc/
:copen
:81
:156
:85
:36
:Qa
:ProjectCreate %:p:h -p hotelsbonus -n php,javascript
:43
:diffoff
:diffth
:difth
:ProjectCreate %:p:h -p PEAR -n php,javascript
:ProjectDelete PEAR
:ProjectDelete pear
:39
:423
:lclose
:50
:qw
:%s/\<self\>/static/gc
:lopen
:%s/\<USER_SALT\>/PASS
:1287
:1284
:args! `grep! 'travelpassport' *` 
:args! `grep! 'travelpassport' *`
:args! `grep! 'travelpassport' * --include=*.{php,js,css}`
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo %s/travelpassport/travelpassport
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo cat
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo
:args! `grep! 'travelpassport' * --include=*.{php,js,css}` | argdo echo
:grep! 'travelpassport' * --include=*.{php,js,css}
:%s/\->travelpassport/->travelpassport
:%s/\v->travelpassport/->travelpassport
:%s/\v-\>travelpassport/->travelpassport
:%s/\v\->travelpassport/->travelpassport
:%s/\<load_tag\>/get_tag/gc
:%s/\<teg_key\>/tag_key/gc
:%s/\\v\\->travelpassport/->travelpassport
:%s/\\v\->travelpassport/->travelpassport
:%s/->travelpassport/->travelpassport
:ProjectCreate %:p:h -p accido -n php,javascript
:ProjectCreate %:p:h -p accido -n php,javascript,html
:ProjectCreate %:p:h -p accido -n php
:ProjectDelete accido
:%s/\<id_tag\>/create_tag/gc
:%s/\<teg_key\>/tag_key
:ProjectNatureAdd accido php
:ProjectNatureAdd php
:ProjectNatureAdd pph
:ProjectNatureAdd 
:EclimDisable
:vnew

# Строка поиска, история (начиная от свежего к старому):
? ^\s*\(5\)\>
?/$_GET
?/para
?/createMetr
?/log
?/default
? ^\s*\(329\)\>
?/min
?/length
?/string
?/validate
?/form
? ^\s*\(318\)\>
? ^\s*\(310\)\>
?/_filter
?/valid
? ^\s*\(57\)\>
?/assert
?/isAdmin
? ^\s*\(40\)\>
?/isad
?/$acl
?/403
?/admin
?/id
?/main
?/translate
? ''
?/passport
?/\/translate
?/\/hotline
?/\/call
?/translate\/tasks
?/translate/tasks
?/debug
?/delete
? ^\s*\(104\)\>
? ^\s*\(75\)\>
?/icache
?/->table
? ^\s*\(116\)\>
? ^\s*\(109\)\>
? ^\s*\(26\)\>
? ^\s*\(23\)\>
?/#action
?/date_promo
? ^\s*\(27\)\>
?/->_parts
? ^\s*\(28\)\>
? ^\s*\(29\)\>
? ^\s*\(24\)\>
? ^\s*\(19\)\>
?/date
?/Date
?/timestamp
? ^\s*\(82\)\>
?/bootst
?/button
?/subm
?/submit
? ^\s*\(279\)\>
? ^\s*\(285\)\>
? ^\s*\(291\)\>
?/summary

# Выражение, история (начиная от свежего к старому):

# Строка ввода, история (начиная от свежего к старому):
@Assert
@assert
@/home/accido/locale/src/stat.hp.com/www/loader.php
@/home/accido/locale/src/hotline.hp.com/www/loader.php
@yes
@/home/accido/locale/src/hotelscheck/app/modules/admin/assets/js/admin.js
@/home/accido/locale/src/hotelscheck/app/modules/admin/assets/js/
@/home/accido/locale/src/hotelscheck/app/components/WebModule.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/subscriber/SummaryAction.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/subscriber/MonthAction.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/subscriber/
@/home/accido/locale/src/hotelscheck/app/components/behaviors/DateBehavior.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/subscriber/index.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/subscriber/
@/home/accido/locale/src/hotelscheck/app/components/StrapModule.php
@/home/accido/locale/src/accido/www/core/cache/driver.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/column2.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/2column,php
@/home/accido/locale/src/hotelscheck/app/modules/admin/controllers/SubscriberController.php
@/home/accido/locale/src/hotelscheck/www/robots.txt
@/home/accido/locale/src/hotelscheck/app/views/layouts/admin.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/main.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/views/layouts/
@/home/accido/locale/src/hotelscheck/app/modules/admin/components/Controller.php
@/home/accido/locale/src/hotelscheck/app/modules/admin/components/
@RecordBehavior
@recordbehavior
@/home/accido/locale/src/hotelscheck/app/components/UserBehavior.php
@->register
@\->register_model
@->travelpassport
@\->travelpassport
@-\>travelpassport
@->_allocation
@\->_allocation
@\-\>travelpassport
@->register_model
@-\>register_model
@/var/www/accido/www/core/type.php
@/var/www/accido/www/core/formatter/gz.php
@/var/www/accido/www/core/config.php
@/home/accido/usr/share/applications/eclipse.desktop
@/var/www/jsfw/www/assets/core/models/test.js
@/home/accido/locale/src/hotelscheck/app/models/AboutForm.php
@/home/accido/locale/src/hotelscheck/app/views/layouts/static.php
@/home/accido/locale/src/hotelscheck/app/components/RecordCommandBuilder.php
@/home/accido/locale/src/hotelscheck/app/components/RecordCommandBuilder,php
@/home/accido/locale/src/hotelscheck/app/views/about/index.php
@/home/accido/locale/src/hotelscheck/app/views/about/
@/home/accido/locale/src/hotelscheck/app/controllers/AboutController.php
@/home/accido/locale/src/hotelscheck/app/views/search/index.php
@/home/accido/locale/src/hotelscheck/app/views/search/
@/home/accido/locale/src/hotelscheck/app/controllers/SearchController.php
@/home/accido/locale/src/hotelscheck/app/views/promo/index.php
@/home/accido/locale/src/hotelscheck/app/views/promo/promo.php
@/home/accido/locale/src/hotelscheck/app/views/promo/
@/home/accido/locale/src/hotelscheck/app/controllers/PromoController.php
@/home/accido/locale/src/hotelscheck/app/views/ajax/info.php
@/home/accido/locale/src/hotelscheck/app/controllers/ajax/InfoAction.php
@/home/accido/locale/src/hotelscheck/app/controllers/ajax/article/SubscribeAction.php
@/home/accido/locale/src/hotelscheck/app/components/behaviors/EmbedBehavior.php
@/home/accido/locale/src/hotelscheck/app/components/behaviors/
@/home/accido/locale/src/hotelscheck/app/components/widgets/Share.php
@/home/accido/locale/src/hotelscheck/app/views/widgets/sort.php

# Строка ввода, история (начиная от свежего к старому):

# Регистры:
"0	LINE	0
	/**
	 * Class: heredoc
	 *
	 * @package Model
	 * 
	 * @author andrew scherbakov <accido@ukr.net>
	 * @version $id$
	 * @copyright © 2014 andrew scherbakov
	 * @license MIT http://opensource.org/licenses/MIT
	 */
""1	LINE	0
	<<<<<<< HEAD
	=======
	
	>>>>>>> 3a042f0... подсказки в редакторе создания публикации
"2	LINE	0
	>>>>>>> added nopayment module with menu
"3	LINE	0
	<<<<<<< HEAD
	.popover-publish, .popover-send-mail, .popover-target{
	    color: #3a87ad;
	}
	
	.popover{
	    max-width: 400px;
	    line-height: 25px;
	=======
"4	LINE	0
	>>>>>>> develop
"5	LINE	0
	<<<<<<< HEAD
	                            'url' => array('/about/default/index'),
	                            'template' => '{menu}<span class="icon-lock"></span>',
	                            'itemOptions' => array(
	                                'class' => 'item-icon-right',
	                            ),
	                        ),
	                        array(
	                            'label' => Yii::t('welcome.about', 'Product blog'),
	                            'url' => array('/about/news/index'),
	                            'active' => Yii::app()->getController()->uniqueId == 'about/news',
	                            'template' => '{menu}<span class="icon-lock"></span>',
	                            'itemOptions' => array(
	                                'class' => 'item-icon-right',
	                            ),
	                        ),
	                        array(
	                            'label' => Yii::t('welcome.about', 'Product articles'),
	                            'url' => array('/about/articles/index'),
	                            'active' => Yii::app()->getController()->uniqueId == 'about/articles',
	                            'template' => '{menu}<span class="icon-lock"></span>',
	                            'itemOptions' => array(
	                                'class' => 'item-icon-right',
	                            ),
	=======
"6	LINE	0
	>>>>>>> develop
"7	LINE	0
	<<<<<<< HEAD
	                            'url' => array('/competitors/default/index'),
	=======
"8	LINE	0
	>>>>>>> develop
"9	LINE	0
	<<<<<<< HEAD
	                            'url' => array('/account/post'),
	=======
"a@	CHAR	0
	1255
<adie('test�kr�kr;�k2dd�k2�k5�kd�kd�kd�kd�kd�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�kdOFs					.fail(function(�kr{�kr�kr;�kd�kd�kd�kd�kd�kd�kd�kd�kd�ku�ku�ku�ku�kdOF�kb				.fail(function(�kr{�kr�kr;�kl�ku�kl�kd�kl�ku�ku�ku�ku�ku�ku�ku�kr�kr�kr�kr					�ku�kr�ku						_this.resst�kb�kbet(�kr;�kd�kd�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�k5�kd�ku�kd�ku:h macro�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku:�kb�k4:h macros�kl�kl�kl�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�k4:h record�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�ku�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�kd�k4
"e	LINE	0
	
	let g:EasyGrepFileAssociations='I:/workspace/vim/vim73/plugin/EasyGrepFileAssociations'
	let g:EasyGrepMode=3
	let g:EasyGrepCommand=1
	let g:EasyGrepRecursive=0
	let g:EasyGrepSearchCurrentBufferDir=0
	let g:EasyGrepIgnoreCase=0
	let g:EasyGrepHidden=0
	let g:EasyGrepFilesToExclude=''
	let g:EasyGrepAllOptionsInExplorer=1
	let g:EasyGrepWindow=0
	let g:EasyGrepReplaceWindowMode=0
	let g:EasyGrepOpenWindowOnMatch=1
	let g:EasyGrepEveryMatch=0
	let g:EasyGrepJumpToMatch=1
	let g:EasyGrepInvertWholeWord=0
	let g:EasyGrepFileAssociationsInExplorer=0
	let g:EasyGrepExtraWarnings=0
	let g:EasyGrepOptionPrefix='<leader>vy'
	let g:EasyGrepReplaceAllPerFile=0
"y	LINE	0
			//$( "#tabs .element.from input.from, #tabs .element.to input.to" ).autocomplete({
				//source:"/includes/cities.php?lang="+lang+"",
				//minLength: 3,
				//delay: 0
			//});
	        $html .= '<a style="' .
	            $border . 'background-color: #' .
	            $color . ' !important;" href="#" title="' .
	            $this->label . '" class="text-center btn btn-small">' .
	            $this->count . '</a>';
"-	CHAR	0
	7

# Глобальные отметки:
'J  818  19  I:\workspace\vim\_vimrc
'K  818  19  I:\workspace\vim\_vimrc
'0  78  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
'1  17  0  ~/.pass/.mypass
'2  1  0  ~/locale/src/hotline.hp.com/.git/__Tagbar__
'3  1  0  ~/locale/src/hotline.hp.com/.git/COMMIT_EDITMSG
'4  1  0  ~/.local/share/Steam/steamapps/common/Half-Life/cstrike
'5  665  0  ~/locale/src/hotline.hp.com/app/runtime/application.log
'6  157  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
'7  162  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
'8  75  24  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/layouts/parts/_nav.php
'9  45  28  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/layouts/parts/_nav.php

# Список прыжков (сначала более свежие):
-'  78  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
-'  1  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/[BufExplorer]
-'  4  0  ~/usr/src/hotelspassport.ru/trunk/code/Hp/Db/Tariff/DirectoryW.php
-'  1  0  ~/usr/src/hotelspassport.ru/trunk/code/Hp/Db/Tariff/[BufExplorer]
-'  67  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
-'  75  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
-'  39  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
-'  9  0  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription.php
-'  251  4  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription.php
-'  86  16  ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription.php
-'  1  0  ~/usr/src/hotelspassport.ru/trunk/www/index.php
-'  1  0  ~/locale/src/hp.dev/code/Hp/Db/User.php
-'  1  0  ~/locale/src/hp.dev/www/index.php
-'  17  0  ~/.pass/.mypass
-'  1  0  ~/.pass/.mypass
-'  1  0  ~/locale/src/hotline.hp.com/.git/__Tagbar__
-'  8  0  ~/locale/src/hotline.hp.com/.git/NERD_tree_1
-'  27  0  ~/locale/src/hotline.hp.com/.git/NERD_tree_1
-'  1  0  ~/locale/src/hotline.hp.com/.git/NERD_tree_1
-'  1  0  ~/locale/src/hotline.hp.com/.git/COMMIT_EDITMSG
-'  11  4  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/layouts/main.php
-'  23  1  ~/locale/src/hotline.hp.com/README.md
-'  1  20  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/layouts/parts/_nav.php
-'  40  8  ~/locale/src/hotline.hp.com/app/config/main.php
-'  158  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  172  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  175  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  1  0  ~/.local/share/Steam/steamapps/common/Half-Life/cstrike
-'  665  0  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  7262  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  6571  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  5880  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  5189  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  3062  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  2263  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  1464  21  ~/locale/src/hotline.hp.com/app/runtime/application.log
-'  157  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  29  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  63  8  ~/locale/src/hotline.hp.com/app/components/Controller.php
-'  139  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  162  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  1  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  75  24  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/layouts/parts/_nav.php
-'  45  28  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/layouts/parts/_nav.php
-'  109  0  ~/locale/src/hotline.hp.com/app/modules/nopayment/views/account/index.php
-'  9  0  ~/locale/src/hotline.hp.com/app/modules/nopayment/controllers/AccountController.php
-'  67  4  ~/locale/src/hotline.hp.com/app/modules/nopayment/controllers/AccountController.php
-'  75  0  ~/locale/src/hotline.hp.com/app/modules/nopayment/NopaymentModule.php
-'  71  12  ~/locale/src/hotline.hp.com/app/config/main.php
-'  7  4  ~/locale/src/hotline.hp.com/app/components/Controller.php
-'  150  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  140  0  ~/locale/src/hotline.hp.com/app/assets/css/project.css
-'  28  45  ~/locale/src/hotline.hp.com/app/modules/nopayment/assets/css/main.css
-'  24  46  ~/locale/src/hotline.hp.com/app/modules/nopayment/assets/css/main.css
-'  10  0  ~/locale/src/hotline.hp.com/app/components/widgets/views/parts/_about.php
-'  1  0  ~/locale/src/hotline.hp.com/app/components/widgets/views/parts/[BufExplorer]
-'  13  11  ~/locale/src/hotline.hp.com/app/components/widgets/views/parts/_about.php
-'  1  0  ~/locale/src/hotline.hp.com/app/modules/nopayment/assets/css/[BufExplorer]
-'  5705  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  1  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/[BufExplorer]
-'  11  0  ~/locale/src/hotline.hp.com/app/components/widgets/views/welcomeInformer.php
-'  1  0  ~/locale/src/hotline.hp.com/app/components/widgets/views/[BufExplorer]
-'  8  0  ~/locale/src/hotline.hp.com/app/components/widgets/views/parts/_about.php
-'  5695  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  5876  10  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  5877  15  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  5888  10  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  5889  23  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  714  6  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  718  7  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  719  7  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  1534  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  44  0  ~/locale/src/hotline.hp.com/app/modules/nopayment/NopaymentModule.php
-'  1  0  ~/locale/src/hotline.hp.com/app/modules/nopayment/[BufExplorer]
-'  1535  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  722  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  5891  32  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  5693  0  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3916  27  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3824  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3784  21  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3488  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3487  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3481  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3480  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3479  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css
-'  3478  5  ~/locale/src/hotline.hp.com/vendor/2amigos/yiistrap/assets/css/bootstrap.css

# История местных отметок (от более свежих к старым):

> ~/usr/src/hotelspassport.ru/trunk/www/index.php
	"	1	0

> ~/locale/src/hp.dev/www/index.php
	"	1	0

> ~/locale/src/hp.dev/code/Hp/Db/User.php
	"	1	0

> ~/usr/src/hotelspassport.ru/trunk/www/NERD_tree_1
	"	97	0
	.	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0
	+	1	0

> ~/usr/src/hotelspassport.ru/trunk/www/__Tagbar__
	"	5	3
	.	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0
	+	6	0

> ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription.php
	"	9	0

> ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/Products.php
	"	78	0

> ~/usr/src/hotelspassport.ru/trunk/code/Hp/Db/Tariff/DirectoryW.php
	"	4	0

> ~/usr/src/hotelspassport.ru/trunk/code/Hp/Db/Tariff/[BufExplorer]
	"	1	0

> ~/usr/src/hotelspassport.ru/trunk/code/Admin/Controller/Subscription/[BufExplorer]
	"	1	0
