/**
 * {{#parameters}}
 * @param {{type}}{{^type}}mixed{{/type}} ${{name}}{{/parameters}}
 * @uses
 * {{#static}}
 * @static{{/static}}
 * @since $id$
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @copyright © 2014 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 *
 * @return
 */
