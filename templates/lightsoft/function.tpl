/**
 * {{#parameters}}@param {{type}}{{^type}}mixed{{/type}} ${{name}}{{/parameters}}
 * @author Andrew Scherbakov <sherbakov@lightsoft.ru>
 * @return mixed
 */
