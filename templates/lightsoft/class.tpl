<?php
/**
 * {{#interface}}Interface: {{/interface}}{{^interface}}Class: {{/interface}}{{name}}
 * @package
 * @subpackage
 * {{#interfaces}}@see {{name}}{{/interfaces}}
 * {{#parent}}@see {{name}}{{/parent}}
 * {{#abstract}}@abstract{{/abstract}}
 * {{#final}}@final{{/final}}
 * @author Andrew Scherbakov <sherbakov@lightsoft.ru>
 */
