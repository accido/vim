<?php
/**
 *  Class: {{name}}
 * @package
 * @subpackage
 * {{#interfaces}}@see {{name}}{{/interfaces}}
 * {{#parent}}@see {{name}}{{/parent}}
 * {{#abstract}}@abstract{{/abstract}}
 * {{#final}}@final{{/final}}
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 */
