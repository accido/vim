let s:sPath = expand('<sfile>:p:h')
let g:indexer_projectsSettingsFilename = s:sPath.'/.vimprojects'
let g:proj_project_filename=s:sPath.'/.vimprojects'
let g:indexer_indexerListFilename=s:sPath.'/.indexer_files'
let g:indexer_ctagsCommandLineOptions='--fields=+iaS --extra=+q --PHP-kinds=+cfidvp --JavaScript-kinds=+cfidvp'
let g:indexer_disableCtagsWarning=1
let g:project_remote_host=''
let g:project_remote_root=''
let g:project_session_name='vim'
if has('win32') || has('win64')
  let g:netrw_ftp_cmd = 'ftp -i -v -s:' . s:sPath . '\MACHINE'
  let g:netrw_scp_cmd = 'pscp -pw '
endif
let g:netrw_silent=0
let g:netrw_use_errorwindow=1
let g:netrw_quiet=0
