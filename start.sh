SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
xfce4-terminal --hide-menubar --hide-borders --fullscreen --hide-toolbar --drop-down \
        -T SY-Worker  --working-directory="~/www/SSYii_worker" -e "bash -ic \"cd ~/www/SSYii_worker ; bash\""\
  --tab -T SY         --working-directory="~/www/SSYii" -e "bash -ic \"cd ~/www/SSYii ; bash\""\
  --tab -T server     --working-directory="~/www/server" -e "bash -ic \"cd ~/www/server ; bash\""\
  --tab -T ssv4       --working-directory="~/www/ssv4" -e "bash -ic \"cd ~/www/ssv4 ; bash\""\
  --tab -T yii        --working-directory="~" -e "bash -ic 'ssh -p 22 andrey@192.168.1.10 ; bash'"\
  --tab -T pony       --working-directory="~" -e "bash -ic 'ssh -p 9922 worker@alpha.omnismain.com ; bash'"\
  --tab -T donkey     --working-directory="~" -e "bash -ic 'ssh -p 9922 worker@orion.omnismain.com ; bash'"\
  --tab -T horse      --working-directory="~" -e "bash -ic 'ssh -p 8822 worker@orion.omnismain.com ; bash'"
