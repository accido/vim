" Vim filetype plugin file
" Language:	php
" Maintainer:	Dan Sharp <dwsharp at users dot sourceforge dot net>
" Last Changed: 20 Jan 2009
" URL:		http://dwsharp.users.sourceforge.net/vim/ftplugin

if exists("b:did_ftplugin") | finish | endif

" Make sure the continuation lines below do not cause problems in
" compatibility mode.
let s:keepcpo= &cpo
set cpo&vim

" Define some defaults in case the included ftplugins don't set them.
let s:undo_ftplugin = ""
let s:browsefilter = "HTML Files (*.html, *.htm)\t*.html;*.htm\n" .
	    \	     "All Files (*.*)\t*.*\n"
let s:match_words = ""

"runtime! ftplugin/html.vim ftplugin/html_*.vim ftplugin/html/*.vim
let b:did_ftplugin = 1

" Override our defaults if these were set by an included ftplugin.
if exists("b:undo_ftplugin")
    let s:undo_ftplugin = b:undo_ftplugin
endif
if exists("b:browsefilter")
    let s:browsefilter = b:browsefilter
endif
if exists("b:match_words")
    let s:match_words = b:match_words
endif
if exists("b:match_skip")
    unlet b:match_skip
endif

" Change the :browse e filter to primarily show PHP-related files.
if has("gui_win32")
    let  b:browsefilter="PHP Files (*.php)\t*.php\n" . s:browsefilter
endif

" ###
" Provided by Mikolaj Machowski <mikmach at wp dot pl>
setlocal include=\\\(require\\\|include\\\)\\\(_once\\\)\\\?
" Disabled changing 'iskeyword', it breaks a command such as "*"
" setlocal iskeyword+=$

if exists("loaded_matchit")
    let b:match_words = '<?php:?>,\<switch\>:\<endswitch\>,' .
		      \ '\<if\>:\<elseif\>:\<else\>:\<endif\>,' .
		      \ '\<while\>:\<endwhile\>,' .
		      \ '\<do\>:\<while\>,' .
		      \ '\<for\>:\<endfor\>,' .
		      \ '\<foreach\>:\<endforeach\>,' .
                      \ '(:),[:],{:},' .
		      \ s:match_words
endif
" ###

"if exists('&omnifunc')
""  setlocal omnifunc=phpcomplete#CompletePHP
"endif

" Section jumping: [[ and ]] provided by Antony Scriven <adscriven at gmail dot com>
let s:function = '\(abstract\s\+\|final\s\+\|private\s\+\|protected\s\+\|public\s\+\|static\s\+\)*function'
let s:class = '\(abstract\s\+\|final\s\+\)*class'
let s:interface = 'interface'
let s:section = '\(.*\%#\)\@!\_^\s*\zs\('.s:function.'\|'.s:class.'\|'.s:interface.'\)'
exe 'nno <buffer> <silent> [[ ?' . escape(s:section, '|') . '?<CR>:nohls<CR>'
exe 'nno <buffer> <silent> ]] /' . escape(s:section, '|') . '/<CR>:nohls<CR>'
exe 'ono <buffer> <silent> [[ ?' . escape(s:section, '|') . '?<CR>:nohls<CR>'
exe 'ono <buffer> <silent> ]] /' . escape(s:section, '|') . '/<CR>:nohls<CR>'

setlocal commentstring=/*%s*/

" Undo the stuff we changed.
let b:undo_ftplugin = "setlocal commentstring< include< omnifunc<" .
	    \	      " | unlet! b:browsefilter b:match_words | " .
	    \	      s:undo_ftplugin

"-------------------------
" PHP настройки
"-------------------------
  
" Полезные "быстрые шаблоны"
" Вывод отладочной информации
iabbrev dbg echo '<pre>';var_dump( );echo '</pre>';
iabbrev tm echo 'Test message in file: '.__FILE__.', on line: '.__LINE__;
iabbrev err ini_set('error_reporting', 'stdout');ini_set('display_errors',1);error_reporting(E_ALL);

" Добавляем поддержку встроенного мануала
autocmd BufNewFile,Bufread *.php,*.php3,*.php4 set keywordprg="call OnlineDoc()"
set dictionary+=$VIMRUNTIME/dict/php

" Проверка синтаксиса PHP
set makeprg=php
" Формат вывода ошибок PHP
set errorformat=%m\ in\ %f\ on\ line\ %l

" Php execute
function! RunPhp()
    let l:filename=expand('%:p')
    let l:command=$VIM.'/php/php -l -d error_reporting=E_ALL -d display_errors=1 -d log_errors=0 '.l:filename
    let l:php_output=<SID>SendAsyncText(l:command)
    let l:php_list=split(l:php_output, "\n")
    cexpr l:php_list
    cwindow
endfunction
command! Check execute RunPhp()


let g:tagbar_type_php = {
    \ 'ctagstype' : 'php',
    \ 'kinds' : [
        \ 'i:interfaces',
        \ 'c:classes',
        \ 'd:constant definitions:0:0',
        \ 'f:functions',
        \ 'j:javascript functions',
    \ ],
\ }

nnoremap <silent> <buffer> <cr> :PhpSearchContext<cr>

" Vim постовляется с достаточно мощным плугином подстветки синтаксиса PHP.
" Помимо прочего он умеет:

" Включаем фолдинг для блоков классов/функций
let php_folding = 0

" Не использовать короткие теги PHP для поиска PHP блоков
let php_noShortTags = 1

" Подстветка SQL внутри PHP строк
let php_sql_query=1

" Подстветка HTML внутри PHP строк
let php_htmlInStrings=1 

" Подстветка базовых функций PHP
let php_baselib = 1

" Restore the saved compatibility options.
let &cpo = s:keepcpo

unlet s:keepcpo
